#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from os import curdir, sep
import cgi
import threading
import time

PORT_NUMBER = 8080

#This class will handles any incoming request from
#the browser 
class myHandler(BaseHTTPRequestHandler):
	#Handler for the GET requests
	def do_GET(self):
		if self.path=="/":
			self.path="/index_example3.html"
		try:
			#Check the file extension required and
			#set the right mime type
			print ("Uploading:" + self.path)
			if '?'in self.path:
				self.path = self.path[:self.path.index('?')]
			sendReply = False
			if self.path.endswith(".html"):
				mimetype='text/html'
				binary = False
				sendReply = True
			if self.path.endswith(".jpg"):
				mimetype='image/jpg'
				binary = True
				sendReply = True
			if self.path.endswith(".gif"):
				mimetype= 'image/gif'
				binary = True
				sendReply = True
			if self.path.endswith(".bmp"):
				mimetype='image/bmp'
				binary = True
				sendReply = True
			if self.path.endswith(".js"):
				mimetype='application/javascript'
				binary = False
				sendReply = True
			if self.path.endswith(".css"):
				mimetype='text/css'
				binary = False
				sendReply = True

			if sendReply == True:
				#Open the static file requested and send it
				if binary:
					f = open(curdir + sep + self.path, "rb")
				else:
					f = open(curdir + sep + self.path, "r")
				print("Send file:" + curdir + sep + self.path)
				if mimetype != None:
					self.send_response(200)
					self.send_header('Content-type',mimetype)
					self.end_headers()
				self.wfile.write(f.read())
				f.close()
			return

		except IOError:
			self.send_error(404,'File Not Found: %s' % self.path)

	#Handler for the POST requests
	def do_POST(self):
		print("Post:" + self.path)
		form = cgi.FieldStorage(
		fp=self.rfile, 
			headers=self.headers,
			environ={'REQUEST_METHOD':'POST',
			 'CONTENT_TYPE':self.headers['Content-Type'],
		})
		print("Click coordinates are: %s,%s" % (form["form_x"].value , form["form_y"].value))
		self.send_response(200)
		self.end_headers()
		f = open("index_example3.html", "r")
		#self.send_response(200)
		#self.send_header('Content-type','text/html')
		#self.end_headers()
		self.wfile.write(f.read())
		f.close()
		return			

def run_webserver_thread():
	try:
		print 'Started httpserver on port ' , PORT_NUMBER
		webserver.serve_forever();
		print 'Ending httpserver' , PORT_NUMBER
	except KeyboardInterrupt:
		print '^C received, shutting down the web server'
		webserver.socket.close()
        

webserver = HTTPServer(('', PORT_NUMBER), myHandler)
webserver_thread = threading.Thread( target=run_webserver_thread)
webserver_thread.start()
counter = 0
while 1:
	time.sleep(1)
	counter += 1
	if counter > 20:
		break;
webserver.shutdown()
	
