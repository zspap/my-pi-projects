# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import texts



class soundSelectScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        self.soundDriver = self.systemDriver.drivers[systemDriver.DRV_SOUND]

        self.buttonOk =  pygbutton.PygButton((260, 0, 60, 60), normal='./artwork/buttonOk60.bmp', down='./artwork/buttonOk60Up.bmp', highlight='./artwork/buttonOk60.bmp', keycolor = (0,0,0))
        self.buttonPlay =  pygbutton.PygButton((200, 60, 60, 60), normal='./artwork/buttonPlay.bmp', down='./artwork/buttonPlayUp.bmp', highlight='./artwork/buttonPlay.bmp', keycolor = (0,0,0))
        self.buttonNext =  pygbutton.PygButton((200, 120, 60, 60), normal='./artwork/buttonNext.bmp', down='./artwork/buttonNextUp.bmp', highlight='./artwork/buttonNext.bmp', keycolor = (0,0,0))
        self.buttonStop =  pygbutton.PygButton((200, 60, 60, 60), normal='./artwork/buttonStop.bmp', down='./artwork/buttonStopUp.bmp', highlight='./artwork/buttonStop.bmp', keycolor = (0,0,0))
        self.buttonRandom =  pygbutton.PygButton((260, 60, 60, 60), normal='./artwork/buttonRnd.bmp', down='./artwork/buttonRndUp.bmp', highlight='./artwork/buttonRnd.bmp', keycolor = (0,0,0))

        self.buttonUp =  pygbutton.PygButton((200, 0, 60, 60), normal='./artwork/buttonUp.bmp', down='./artwork/buttonUpUp.bmp', highlight='./artwork/buttonUp.bmp', keycolor = (0,0,0))
        self.buttonDown =  pygbutton.PygButton((200, 180, 60, 60), normal='./artwork/buttonDown.bmp', down='./artwork/buttonDownUp.bmp', highlight='./artwork/buttonDown.bmp', keycolor = (0,0,0))

        self.buttonPlus = pygbutton.PygButton((260, 120, 60, 60), normal='./artwork/buttonPlus.bmp', down='./artwork/buttonPlusUp.bmp', highlight='./artwork/buttonPlus.bmp', keycolor = (0,0,0))
        self.buttonMinus = pygbutton.PygButton((260, 180, 60, 60), normal='./artwork/buttonMinus.bmp', down='./artwork/buttonMinusUp.bmp', highlight='./artwork/buttonMinus.bmp', keycolor = (0,0,0))
        self.background = pygame.image.load('./artwork/backgroundMusic.bmp')
        self.background.set_alpha(128)

        # UP    OK
        # PL/St RND
        # Nx    +
        # Dn    -

        self.CurrentDir = None
        self.CurDirIndex = 0
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 28)
        self.font1 = pygame.font.SysFont(None, 30)

        self.autoCloseCounter = 1600

    def InitScreen(self):
        d= './Music'
        self.DirectoryList = [o for o in os.listdir(d) if os.path.isdir(os.path.join(d,o))] # os.path.join(d,o)
        if self.DirectoryList:
            self.CurrentDir = self.DirectoryList[0]
        else:
            self.CurrentDir = None
        
    def OnDraw(self, surface):
        surface.blit(self.background, (0, 0))
        # Draw List
        pygame.draw.rect(surface, (255, 255, 25), (0, 0, 190, 240), 1)
        corr = 3
        index2 = 0
        for index in range((self.CurDirIndex / 8 ) * 8, (self.CurDirIndex / 8 ) * 8 + 8):
            if index >= len(self.DirectoryList):
                break
            if index == self.CurDirIndex:
                label = self.font1.render(self.DirectoryList[index], 1, (255, 255, 255))
                surface.blit(label, (0, index2 * 28 + corr))
                corr = 5
            else:
                label = self.font0.render(self.DirectoryList[index], 1, (255, 55, 55))
                surface.blit(label, (0, index2 * 28 + corr))
            index2 += 1

        # Draw Buttons
        self.buttonMinus.draw(surface)
        self.buttonPlus.draw(surface)
        self.buttonOk.draw(surface)
        if self.soundDriver.GetState():
            self.buttonStop.draw(surface)
            self.buttonNext.draw(surface)
        else:
            self.buttonPlay.draw(surface)
            self.buttonRandom.draw(surface)
        self.buttonUp.draw(surface)
        self.buttonDown.draw(surface)

        # Draw Volume
        
        self.autoCloseCounter -= 1
        if self.autoCloseCounter < 0:
             pygame.event.post(pygame.event.Event(USEREVENT, {'x': 1, 'y':1}))
        # Draw Valume Value
        #label = self.font0.render("Rendszerhiba!", 1, (255, 55, 55))
        #surface.blit(label, (64, 4))
        #label = self.font0.render(self.ErrorMessage, 1, (255, 255, 255))
        #surface.blit(label, (32, 34))
        #surface.blit(self.iconFault, (100, 80))
        #surface.set_colorkey((0,0,0))
             
    def OnEvent(self, event):
        if event.type == USEREVENT:
            self.autoCloseCounter = 1600
            return graphDriver.SC_DRIVER_MAIN
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.autoCloseCounter = 1600
            relx, rely = pygame.mouse.get_pos()
            if relx < 200:
                base = (self.CurDirIndex / 8 ) * 8
                self.CurDirIndex = base + (rely / 30)
                #print("Rely:"+ str(rely) + "  Index:" + str(self.CurDirIndex) + " Base:" + str(base))
                if self.CurDirIndex >= len(self.DirectoryList):
                    self.CurDirIndex = len(self.DirectoryList) - 1
        if 'click' in self.buttonOk.handleEvent(event):
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonDown.handleEvent(event):
            self.autoCloseCounter = 1600
            self.CurDirIndex += 1
            if self.CurDirIndex >= len(self.DirectoryList):
                self.CurDirIndex = 0
        if 'click' in self.buttonUp.handleEvent(event):
            self.autoCloseCounter = 1600
            self.CurDirIndex -= 1
            if self.CurDirIndex < 0:
                self.CurDirIndex = len(self.DirectoryList) - 1
        if self.soundDriver.GetState():
            if 'click' in self.buttonStop.handleEvent(event):
                self.autoCloseCounter = 1600
                self.soundDriver.Play(0, None, 0)
            if 'click' in self.buttonNext.handleEvent(event):
                self.autoCloseCounter = 1600
                self.soundDriver.Play(2, None, 0)
        else:
            if 'click' in self.buttonRandom.handleEvent(event):
                self.autoCloseCounter = 1600
                self.soundDriver.Play(1, './Music/' +self.DirectoryList[self.CurDirIndex] + '/', 1)
            if 'click' in self.buttonPlay.handleEvent(event):
                self.autoCloseCounter = 1600
                self.soundDriver.Play(1, './Music/' +self.DirectoryList[self.CurDirIndex] + '/', 0)

        if 'click' in self.buttonPlus.handleEvent(event):
            self.autoCloseCounter = 1600
            vol = self.soundDriver.GetVolume()
            vol += 5
            if vol > 100:
                vol = 100
            self.soundDriver.SetVolume(vol)
        if 'click' in self.buttonMinus.handleEvent(event):
            self.autoCloseCounter = 1600
            vol = self.soundDriver.GetVolume()
            vol -= 5
            if vol < 0:
                vol = 0
            self.soundDriver.SetVolume(vol)
        return 0


