import time
import pigpio
import hwDriverGeneralClass

class lampDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for normal sauna lamp"""
    def __init__(self, pi, gpio):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.pi = pi
        self.gpio = gpio
        self.pi.set_mode(gpio, pigpio.OUTPUT)
        self.currentState = 0    # 0: OFF.  1: ON
        self.timerCounter = 0

    def OnTick(self):
        if self.timerCounter > 0:
            self.timerCounter = self.timerCounter -1
            if self.timerCounter == 0:
                self.SwitchOn(0)
                return 0
            return 1
        return 0
                
    def IsHWOK(self):
        return True

    def SwitchOn(self, on, timing = 0):
        self.currentState = on
        if on == 0:
            self.pi.write(self.gpio, 0)
            self.timerCounter = 0
        else:
            self.pi.write(self.gpio, 1)
            self.timerCounter = timing * 2

    def GetState(self):
        return self.currentState
