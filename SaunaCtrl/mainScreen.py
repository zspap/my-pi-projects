# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import datetime
import pygbutton
import setValueScreen
import closeDoorScreen
import lightSetupScreen
import texts
import logger 

class mainScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        self.saunaDrv = self.systemDriver.drivers[systemDriver.DRV_SAUNA]

        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 49)
        self.font1 = pygame.font.Font(None, 30)
        self.font2 = pygame.font.Font(None, 100)
        self.font3 = pygame.font.Font(None, 60)
        self.iconbar = pygame.surface.Surface((320,60))
        self.iconbar.fill((0,0,0))
        self.iconbar.set_alpha(128)

        self.doorOpenCounter = 0
        
        self.buttonStop = pygbutton.PygButton((0, 0, 125, 125), normal='./artwork/iconStopUp.bmp', down='./artwork/iconStopDown.bmp', highlight='./artwork/iconStopUp.bmp', keycolor = (0,0,0))
        self.buttonLight = pygbutton.PygButton((0, 180, 60, 60), normal='./artwork/buttonLamp.bmp', down='./artwork/buttonLampOn.bmp', highlight='./artwork/buttonLamp.bmp', keycolor = (0,0,0))
        self.buttonTemp =  pygbutton.PygButton((65, 180, 60, 60), normal='./artwork/buttonTemp.bmp', down='./artwork/buttonTempOn.bmp', highlight='./artwork/buttonTemp.bmp', keycolor = (0,0,0))
        self.buttonTime =  pygbutton.PygButton((130, 180, 60, 60), normal='./artwork/buttonTime.bmp', down='./artwork/buttonTimeOn.bmp', highlight='./artwork/buttonTime.bmp', keycolor = (0,0,0))
        self.buttonMusic = pygbutton.PygButton((195, 180, 60, 60), normal='./artwork/buttonMusic.bmp', down='./artwork/buttonMusicOn.bmp', highlight='./artwork/buttonMusic.bmp', keycolor = (0,0,0))
        self.buttonVent = pygbutton.PygButton((260, 180, 60, 60), normal='./artwork/buttonVent.bmp', down='./artwork/buttonVentOn.bmp', highlight='./artwork/buttonVent.bmp', keycolor = (0,0,0))

        self.iconVentON = pygame.image.load('./artwork/iconVentOn.bmp')
        self.iconVentON.set_colorkey((0,0,0))
        self.iconVentOFF = pygame.image.load('./artwork/iconVentOn.bmp')
        self.iconVentOFF.set_alpha(20)

        self.iconLampON = pygame.image.load('./artwork/iconLampOn.bmp')
        self.iconLampON.set_colorkey((0,0,0))
        self.iconLampOFF = pygame.image.load('./artwork/iconLampOn.bmp')
        self.iconLampOFF.set_alpha(20)

        self.iconRGBON = pygame.image.load('./artwork/iconRGBOn.bmp')
        self.iconRGBON.set_colorkey((0,0,0))
        self.iconRGBOFF = pygame.image.load('./artwork/iconRGBOn.bmp')
        self.iconRGBOFF.set_alpha(20)

        self.iconInfraON = pygame.image.load('./artwork/iconInfraOn.bmp')
        self.iconInfraON.set_colorkey((0,0,0))
        self.iconInfraOFF = pygame.image.load('./artwork/iconInfraOn.bmp')
        self.iconInfraOFF.set_alpha(20)

        self.iconHeaterON = pygame.image.load('./artwork/iconHeaterOn.bmp')
        self.iconHeaterON.set_colorkey((0,0,0))
        self.iconHeaterOFF = pygame.image.load('./artwork/iconHeaterOn.bmp')
        self.iconHeaterOFF.set_alpha(20)

        self.iconMusicON = pygame.image.load('./artwork/iconMusicOn.bmp')
        self.iconMusicON.set_colorkey((0,0,0))
        self.iconMusicOFF = pygame.image.load('./artwork/iconMusicOn.bmp')
        self.iconMusicOFF.set_alpha(20)

        self.iconPumpON = pygame.image.load('./artwork/iconPumpOn.bmp')
        self.iconPumpON.set_colorkey((0,0,0))
        self.iconPumpOFF = pygame.image.load('./artwork/iconPumpOn.bmp')
        self.iconPumpOFF.set_alpha(20)

        self.iconDoorON = pygame.image.load('./artwork/iconDoorOn.bmp')
        self.iconDoorON.set_colorkey((0,0,0))
        self.iconDoorOFF = pygame.image.load('./artwork/iconDoorOn.bmp')
        self.iconDoorOFF.set_alpha(20)

        self.basicBackground = pygame.image.load('./artwork/background.bmp')
        self.basicBackground.set_alpha(200)

    def OnDraw(self, surface):
        # Background image
        surface.blit(self.basicBackground, (0, 0))
        
        # Draw temperature data
        temp1 = self.saunaDrv.GetTemperature()
        hum1 = self.saunaDrv.GetHumidity()
        state = self.saunaDrv.GetStatTxt()
        timetg = self.saunaDrv.GetTimeToGo()
        diff = self.saunaDrv.GetTempDiff()
        # Draw Temp and Hum.
        if diff == -2:
            color = (64, 64, 255)
        elif (diff == -1) or (diff == 1):
            color = (255, 255, 32)
        elif diff == 0:
            color = (255, 255, 255)
        else:
            color = (255, 64, 64)
        if temp1 >= 100:
            temp_text = "{:.0f}".format(temp1) + texts.text_celsius1 + "{:.0f}".format(hum1) + "%"
        else:
            temp_text = "{:.1f}".format(temp1) + texts.text_celsius1 + "{:.0f}".format(hum1) + "%"
        label = self.font0.render(temp_text, 1, color)
        surface.blit(label, (133, 0))
        # Draw State data
        label = self.font1.render(state, 1, (255,255, 255))
        surface.blit(label, (133, 42))
        # Draw Time To Go
        a = datetime.timedelta(seconds = timetg)
        label = self.font0.render(str(a), 1, (255,255, 255))
        surface.blit(label, (133, 72))
        # Draw Icons: Vent, LED, Lamp, Heater, Infra, Music, Pump, Door
        surface.blit(self.iconbar, (0, 131))

        if self.systemDriver.drivers[systemDriver.DRV_VENT].GetState():
            surface.blit(self.iconVentON, (282, 131))
        else:
            surface.blit(self.iconVentOFF, (282, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_LAMP].GetState():
            surface.blit(self.iconLampON, (2, 131))
        else:
            surface.blit(self.iconLampOFF, (2, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_RGB].GetState():
            surface.blit(self.iconRGBON, (42, 131))
        else:
            surface.blit(self.iconRGBOFF, (42, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_INFRA].GetState():
            surface.blit(self.iconInfraON, (82, 131))
        else:
            surface.blit(self.iconInfraOFF, (82, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_HEATER].GetState():
            surface.blit(self.iconHeaterON, (122, 131))
        else:
            surface.blit(self.iconHeaterOFF, (122, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_SOUND].GetState():
            surface.blit(self.iconMusicON, (202, 131))
        else:
            surface.blit(self.iconMusicOFF, (202, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_PUMP].GetState():
            surface.blit(self.iconPumpON, (162, 131))
        else:
            surface.blit(self.iconPumpOFF, (162, 131))   
        if self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState():
            surface.blit(self.iconDoorON, (242, 131))
            self.doorOpenCounter += 1
        else:
            surface.blit(self.iconDoorOFF, (242, 131))
            self.doorOpenCounter = 0
            
        # Draw kWh data
        cons1 = self.systemDriver.drivers[systemDriver.DRV_SAUNA].GetConsumption()
        money1 = self.systemDriver.drivers[systemDriver.DRV_SAUNA].GetMoney()
        temp_text = "{:.1f}".format(cons1) + texts.text_kwh1 + "{:.0f}".format(money1) + texts.text_ft
        label = self.font1.render(temp_text, 1, (128,128, 128))
        surface.blit(label, (133, 110))
          
        # Draw buttons
        self.buttonStop.draw(surface)
        self.buttonLight.draw(surface)
        if self.saunaDrv.GetSaunaMode() != saunaControl.MODE_CLEANING:
            self.buttonTemp.draw(surface)
        self.buttonTime.draw(surface)
        self.buttonMusic.draw(surface)
        self.buttonVent.draw(surface)

        # Check door
        if self.doorOpenCounter > 640:
            self.doorOpenCounter = 0
            pygame.event.post(pygame.event.Event(USEREVENT, {'x': 1, 'y':1}))
        
        #Check Sauna controller State
        if self.saunaDrv.GetSaunaMode() == 0:
            self.StopSauna()

    def OnEvent(self, event):
        if event.type == USEREVENT:
            # Door open State
            return graphDriver.SC_DRIVER_CLOSEDOOR
        if 'click' in self.buttonStop.handleEvent(event): 
            logger.MainLogger.LogEvent("Manual STOP button pushed. Stop Sauna.")
            self.StopSauna()
            return graphDriver.SC_DRIVER_OFF
        if 'click' in self.buttonTemp.handleEvent(event):
            self.screenDriver.drivers[graphDriver.SC_DRIVER_SETPARAM].InitScreen(texts.text_temperature, setValueScreen.SET_PARAM_MODE_TEMP, 22, 110, self.saunaDrv.GetRefTemperature(), 1, self.saunaDrv.GetSaunaMode())                                                                        
            return graphDriver.SC_DRIVER_SETPARAM
        if 'click' in self.buttonTime.handleEvent(event):
            self.screenDriver.drivers[graphDriver.SC_DRIVER_SETPARAM].InitScreen(texts.text_timetogo, setValueScreen.SET_PARAM_MODE_TIME, 10, 3*3600, self.saunaDrv.GetTimeToGo(), 330, self.saunaDrv.GetSaunaMode())                                                                        
            return graphDriver.SC_DRIVER_SETPARAM
        if 'click' in self.buttonVent.handleEvent(event):
            if self.systemDriver.drivers[systemDriver.DRV_VENT].GetState():
                self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(0)
            else:
                self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2)
            return 0
        if 'click' in self.buttonLight.handleEvent(event):
            rgbdrv = self.systemDriver.drivers[systemDriver.DRV_RGB]
            self.screenDriver.drivers[graphDriver.SC_DRIVER_LIGHT].InitScreen(rgbdrv.GetState(), rgbdrv.R, rgbdrv.G, rgbdrv.B, self.systemDriver.drivers[systemDriver.DRV_LAMP].GetState())
            return graphDriver.SC_DRIVER_LIGHT
        if 'click' in self.buttonMusic.handleEvent(event):
            self.screenDriver.drivers[graphDriver.SC_DRIVER_SOUNDSETUP].InitScreen()
            return graphDriver.SC_DRIVER_SOUNDSETUP
        return 0

    def StopSauna(self):
        self.saunaDrv.SwitchOff()
        self.systemDriver.drivers[systemDriver.DRV_SOUND].Play(0)
        self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(0)
        self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(0)
        self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2)
