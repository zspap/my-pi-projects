import time
import pigpio
import hwDriverGeneralClass

VENT_TIME = 60     # 60 seconds

class ventDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for normal sauna vent"""
    def __init__(self, pi, gpio):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.pi = pi
        self.gpio = gpio
        self.pi.set_mode(gpio, pigpio.OUTPUT)
        self.currentState = 0    # 0: OFF.  1: ON   2:Timed ON
        self.operationTimer = 0

    def OnTick(self):
        if self.operationTimer > 0:
            # Timed mode
            self.operationTimer = self.operationTimer - 1
            if self.operationTimer == 0:
                # Time End, switch OFF vent.
                self.SwitchOn(0)
                
        return self.currentState
        
    def IsHWOK(self):
        return True

    def SwitchOn(self, on, timer = VENT_TIME):
        self.currentState = on
        if on == 0:
            self.pi.write(self.gpio, 0)
        elif on == 2:
            self.pi.write(self.gpio, 1)
            self.operationTimer = VENT_TIME * 2
        else:
            self.pi.write(self.gpio, 1)
            self.operationTimer = timer * 2

    def GetState(self):
        return self.currentState
    
