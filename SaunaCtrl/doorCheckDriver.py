import time
import pigpio
import hwDriverGeneralClass

class doorCheckDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for door open sensor"""
    def __init__(self, pi, gpio):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.pi = pi
        self.gpio = gpio
        self.pi.set_mode(gpio, pigpio.INPUT)
        self.pi.set_pull_up_down(self.gpio, pigpio.PUD_UP) 
        self.currentState = False        # Closed
        self.openTime = 0

    def OnTick(self):
        oldstate = self.currentState
        self.currentState = self.pi.read(self.gpio)
        if oldstate != self.currentState:
           # Read again to avoid noise.
           time.sleep(0.100)
           self.currentState = self.pi.read(self.gpio)

        if self.currentState:
            self.openTime = self.openTime + 1
        else:
            self.openTime = 0
        
    def IsHWOK(self):
        return True

    def GetState(self):
        return self.currentState

    def GetOpenStateTime(self):
        return self.openTime
    
     
