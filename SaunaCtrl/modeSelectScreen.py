
# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import pygbutton
import texts   
import logger


class modeSelectScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        self.saunaDrv = self.systemDriver.drivers[systemDriver.DRV_SAUNA]

        self.buttonCancel = pygbutton.PygButton((2, 1, 155, 55),  texts.text_cancel, bgcolor=(0,0,255), fgcolor= (255,255,255), normalMap = './artwork/buttonBackCold.bmp')
        self.buttonMode1 = pygbutton.PygButton((160, 1, 155, 55), texts.text_cleaning, bgcolor=(0,0,128), fgcolor= (0,0,0), normalMap = './artwork/buttonBackWater.bmp')

        self.buttonMode2 = pygbutton.PygButton((2, 61, 155, 55),   texts.text_sanarium, bgcolor=(160,64,64), fgcolor= (255,255,255), normalMap = './artwork/buttonBackFire0.bmp')
        self.buttonMode3 =  pygbutton.PygButton((2, 121, 155, 55), texts.text_finnish1, bgcolor=(160,64,64), fgcolor= (255,255,255), normalMap = './artwork/buttonBackFire1.bmp')
        self.buttonMode4 =  pygbutton.PygButton((2, 181, 155, 55), texts.text_finnish2, bgcolor=(160,64,64), fgcolor= (255,255,255), normalMap = './artwork/buttonBackFire2.bmp')

        self.buttonMode5 = pygbutton.PygButton((160, 61, 155, 55),  texts.text_infra2, bgcolor=(160,32,0), fgcolor= (255,255,255), normalMap = './artwork/buttonBackInfra0.bmp')
        self.buttonMode6 = pygbutton.PygButton((160, 121, 155, 55), texts.text_csilla, bgcolor=(160,32,0), fgcolor= (255,255,255), normalMap = './artwork/buttonBackInfra1.bmp')
        self.buttonMode7 = pygbutton.PygButton((160, 181, 155, 55), texts.text_diagnostics, bgcolor=(0,0,0), fgcolor= (32,32,32))

        self.autoCloseCounter = 1600
        self.diagButtonCounter = 0
        
    def OnDraw(self, surface):
        # Draw buttns
        self.buttonCancel.draw(surface)
        self.buttonMode1.draw(surface)
        self.buttonMode2.draw(surface)
        self.buttonMode3.draw(surface)
        self.buttonMode4.draw(surface)
        self.buttonMode5.draw(surface)
        self.buttonMode6.draw(surface)
        self.buttonMode7.draw(surface)
        # Auto close: back to main screen if no activity
        self.autoCloseCounter -= 1
        if self.autoCloseCounter < 0:
             pygame.event.post(pygame.event.Event(USEREVENT, {'x': 1, 'y':1}))
        # Secret code for diagnostics mode: press 4-5 times fast.
        if self.diagButtonCounter > 0:
            self.diagButtonCounter -= 1

    def OnEvent(self, event):
        if event.type == USEREVENT:
            self.autoCloseCounter = 1600
            return graphDriver.SC_DRIVER_OFF
        if 'click' in self.buttonCancel.handleEvent(event):
            return graphDriver.SC_DRIVER_OFF
        if 'click' in self.buttonMode1.handleEvent(event):
            self.saunaDrv.StartSauna(saunaControl.MODE_CLEANING, 22, 600)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(1)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(2,255,255,255)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2, 600)
            self.saunaDrv.ClearConsumption()
            logger.MainLogger.LogEvent("Start Cleaning Mode.")
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonMode2.handleEvent(event):
            self.saunaDrv.StartSauna(saunaControl.MODE_HEAT, 50)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(1)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(1,255,255,255)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2, 60)
            self.saunaDrv.ClearConsumption()
            logger.MainLogger.LogEvent("Start Heat 50 Mode.")
            if self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState():
                return graphDriver.SC_DRIVER_CLOSEDOOR
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonMode3.handleEvent(event):
            self.saunaDrv.StartSauna(saunaControl.MODE_HEAT, 70)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(1)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(2,255,0,0)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2, 60)
            self.saunaDrv.ClearConsumption()
            logger.MainLogger.LogEvent("Start Heat 70 Mode.")
            if self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState():
                return graphDriver.SC_DRIVER_CLOSEDOOR
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonMode4.handleEvent(event):
            self.saunaDrv.StartSauna(saunaControl.MODE_HEAT, 90)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(1,128,0,0)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2, 60)
            self.saunaDrv.ClearConsumption()
            logger.MainLogger.LogEvent("Start Heat 90 Mode.")
            if self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState():
                return graphDriver.SC_DRIVER_CLOSEDOOR
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonMode5.handleEvent(event):
            self.saunaDrv.StartSauna(saunaControl.MODE_INFRA, 60)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(1,255,255,255)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2, 60)
            self.saunaDrv.ClearConsumption()
            logger.MainLogger.LogEvent("Start Infra 35 Mode.")
            if self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState():
                return graphDriver.SC_DRIVER_CLOSEDOOR
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonMode6.handleEvent(event):
            self.saunaDrv.StartSauna(saunaControl.MODE_CSILLA, 100)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(1,255,255,255)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(2, 60)
            self.saunaDrv.ClearConsumption()
            logger.MainLogger.LogEvent("Start Heat 60 Mode.")
            if self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState():
                return graphDriver.SC_DRIVER_CLOSEDOOR
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonMode7.handleEvent(event):
            self.diagButtonCounter += 32
            if self.diagButtonCounter > 100:
                self.diagButtonCounter = 0
                self.saunaDrv.StartSauna(saunaControl.MODE_DIAG)
                logger.MainLogger.LogEvent("Start Diagnostics Mode.")
                return graphDriver.SC_DRIVER_DIAG   
        return 0
