import time
import hwDriverGeneralClass
import pygame, pygbutton, sys
import logger
from pygame.locals import *
from os import listdir
from os.path import isfile, join
from random import randrange

class soundDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for sound subsystem"""
    def __init__(self):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.actualFile = None
        self.fileList = []
        
        self.RandomMix = 0
        self.AudioInitied = True;
        self.InitMusicSystem(False)

    def InitMusicSystem(self, on):
        if on:
            if not self.AudioInitied:
                pygame.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=8192)
                pygame.mixer.init(frequency=44100, size=-16, channels=2, buffer=8192)
                pygame.mixer.music.set_endevent(USEREVENT + 1)
                #pygame.mixer.music.set_volume(1)
                self.AudioInitied = True
        else:
            if self.AudioInitied:        
                pygame.mixer.quit()
                self.AudioInitied = False
        
    def Specevent(self, event):
        if event:
            # End of current play. Jump to the next.
            if (self.actualFile != None) and (self.fileList):
               self.Play(3)
   
    def OnTick(self):
        if self.actualFile == None:
            return 0
        else:
            return 1
        
    def IsHWOK(self):
        return True

    def Play(self, control, directory = None, random = False):
        #  control: 0:Stop  1:Play  2:NextPlay
        self.RandomMix = random
        if directory != None:
            # New filelist given. Read directory
            tempList = [ f for f in listdir(directory) if isfile(join(directory,f)) ]
            self.fileList = []
            if tempList:
                # List is not empty. Check elements and sort files
                for file in tempList:
                    if ".ogg" in file.lower():
                        # it is an ogg file.
                        self.fileList.append(directory + file)
                        print directory + file
                self.fileList.sort()
                if random:
                        self.RandomizeFileList()
            else:
                # List is empty.
                self.actualFile = None
        if self.fileList:
            # Proceed command
            if control == 0:
                if self.actualFile != None:
                    self.actualFile = None;
                    if self.AudioInitied:
                        pygame.mixer.music.stop()
                    self.InitMusicSystem(False)
            elif control == 1:
                self.actualFile = self.fileList[0]
                self.InitMusicSystem(True)
                pygame.mixer.music.load(self.actualFile)
                pygame.mixer.music.play()
            elif control == 2:
                if self.AudioInitied:
                    pygame.mixer.music.stop()    
                self.InitMusicSystem(False)
            else:
                temp = self.fileList[0]
                self.fileList.pop(0)
                self.fileList.append(temp)
                self.actualFile = self.fileList[0]
                self.InitMusicSystem(True)
                pygame.mixer.music.load(self.actualFile)
                pygame.mixer.music.play()
                
    def RandomizeFileList(self):
        for i in range(1,200):
            rnd = randrange(len(self.fileList))
            temp = self.fileList.pop(rnd)
            self.fileList.append(temp)

    def GetState(self):
        if self.actualFile != None:
            return 1
        else:
            return 0

    def GetVolume(self):
        self.InitMusicSystem(True)
        return pygame.mixer.music.get_volume() * 100

    def SetVolume(self, vol):
        self.InitMusicSystem(True)
        return pygame.mixer.music.set_volume(vol / 100.0)

    def GetRandom(self):
        return self.RandomMix
