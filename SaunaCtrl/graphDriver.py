import os
import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import hwDriverGeneralClass
import offScreen
import mainScreen
import modeSelectScreen
import setValueScreen
import closeDoorScreen
import lightSetupScreen
import faultScreen
import diagScreen
import systemDriver
import soundSelectScreen
import platform


FPS = 16
WINDOWWIDTH = 320
WINDOWHEIGHT = 240

SC_DRIVER_OFF  = 1
SC_DRIVER_MODESELECT = 2
SC_DRIVER_MAIN = 3
SC_DRIVER_DIAG = 4
SC_DRIVER_SETPARAM = 5
SC_DRIVER_CLOSEDOOR = 6
SC_DRIVER_LIGHT = 7
SC_DRIVER_FAULT = 8
SC_DRIVER_SOUNDSETUP = 9


class GraphDriver(hwDriverGeneralClass.hwDriverGeneralClass):
    def __init__(self, sysDrv):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        pygame.display.init()
        self.SystemDriver = sysDrv
        
        self.FpsClock = pygame.time.Clock()
        if platform.system() != 'Windows':
            self.DisplaySurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), pygame.FULLSCREEN)
        else:
            self.DisplaySurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT),0)
        self.SecondarySurface = self.DisplaySurface.copy()
        pygame.display.set_caption('Sauna Control Simu')
        self.StopOperation = 0
        self.CurrentScreen = None
        self.drivers = [None] * 10
        self.drivers[0] = None
        self.drivers[SC_DRIVER_OFF] = offScreen.offScreen(self, sysDrv)
        self.drivers[SC_DRIVER_MODESELECT] = modeSelectScreen.modeSelectScreen(self, sysDrv)
        self.drivers[SC_DRIVER_MAIN] = mainScreen.mainScreen(self, sysDrv)
        self.drivers[SC_DRIVER_DIAG] = diagScreen.diagScreen(self, sysDrv)
        self.drivers[SC_DRIVER_SETPARAM] = setValueScreen.setValueScreen(self, sysDrv)
        self.drivers[SC_DRIVER_CLOSEDOOR] = closeDoorScreen.closeDoorScreen(self, sysDrv)
        self.drivers[SC_DRIVER_LIGHT] = lightSetupScreen.lightSetupScreen(self, sysDrv)
        self.drivers[SC_DRIVER_FAULT] = faultScreen.faultScreen(self, sysDrv)
        self.drivers[SC_DRIVER_SOUNDSETUP] = soundSelectScreen.soundSelectScreen(self, sysDrv)
        self.CurrentScreen = self.drivers[SC_DRIVER_OFF]

    def Destroy(self):
        pass

    def GetMainSurface(self):
        return self.SecondarySurface
        
    def doScreenEffect(self):
        overlay = pygame.Surface(self.DisplaySurface.get_size())
        overlay.fill((0,0,0))
        copisurf = self.DisplaySurface.copy()

        for alpha in xrange(0, 255, 32):
            overlay.set_alpha(alpha)
            self.DisplaySurface.blit(copisurf, (0,0))
            self.DisplaySurface.blit(overlay, (0,0))
            pygame.display.update()
            self.FpsClock.tick(FPS)           

        self.DisplaySurface.fill((0, 0, 0))
        if self.CurrentScreen != None:
            self.CurrentScreen.OnDraw(self.DisplaySurface)
        copisurf = self.DisplaySurface.copy()

        for alpha in xrange(255, 0, -32):
            overlay.set_alpha(alpha)
            self.DisplaySurface.blit(copisurf, (0,0))
            self.DisplaySurface.blit(overlay, (0,0))
            pygame.display.update()
            self.FpsClock.tick(FPS)   
            
    def MainScreenLoopStep(self):
        eventlist = pygame.event.get()
        for event in eventlist: # event handling loop
            if event.type == USEREVENT +1:
                # Pygame Sund Driver Event
                self.SystemDriver.drivers[systemDriver.DRV_SOUND].Specevent(event)
            elif event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                return -1
            elif self.CurrentScreen != None:
                ret = self.CurrentScreen.OnEvent(event)
                if ret < -1:
                    return -1
                elif ret == 0:
                    pass
                else:    # Goto New Page
                    self.CurrentScreen = self.drivers[ret]
                    self.doScreenEffect()
                    break

        self.DisplaySurface.fill((0, 0, 0))
           
        if self.CurrentScreen != None:
            self.CurrentScreen.OnDraw(self.DisplaySurface)
            
        self.SecondarySurface.blit(self.DisplaySurface, (0,0))
        pygame.display.update()
        self.FpsClock.tick(FPS)

        return 0
    def OnTick(self):
        return 0

    def IsHWOK(self):
        return True

    def WriteFault(self, errormsg):
        self.drivers[SC_DRIVER_FAULT].InitScreen(errormsg)
        self.CurrentScreen = self.drivers[SC_DRIVER_FAULT];
        self.doScreenEffect()
        



