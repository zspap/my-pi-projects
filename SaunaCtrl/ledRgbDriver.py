import time
import pigpio
import hwDriverGeneralClass

class ledRgbDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for normal sauna vent"""
    def __init__(self, pi, gpioR, gpioG, gpioB):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.pi = pi
        self.gpioR = gpioR
        self.gpioG = gpioG
        self.gpioB = gpioB
        self.pi.set_mode(gpioR, pigpio.OUTPUT)
        self.pi.set_mode(gpioG, pigpio.OUTPUT)
        self.pi.set_mode(gpioB, pigpio.OUTPUT)
        self.pi.set_PWM_frequency(gpioR, 100)
        self.pi.set_PWM_frequency(gpioG, 100)
        self.pi.set_PWM_frequency(gpioB, 100)
        self.pi.set_PWM_dutycycle(gpioR, 0)
        self.pi.set_PWM_dutycycle(gpioG, 0)
        self.pi.set_PWM_dutycycle(gpioB, 0)
        
        self.currentState = 0    # 0: OFF.  1: Automatic mode  2:Manual mode
        self.autoState = 0
        self.R = 0
        self.G = 0
        self.B = 0

    def OnTick(self):
        if self.currentState == 1:
            if self.autoState == 0:     #R
                if self.R > 0:
                    self.R = self.R - 1
                    self.G = self.G + 1
                else:
                    self.autoState = 1
            elif self.autoState == 1:   #G
                if self.G > 0:
                    self.G = self.G - 1
                    self.B = self.B + 1
                else:
                    self.autoState = 2                
            else:                        #B
                if self.B > 0:
                    self.B = self.B - 1
                    self.R = self.R + 1
                else:
                    self.autoState = 0
            self.pi.set_PWM_dutycycle(self.gpioR, self.R)
            self.pi.set_PWM_dutycycle(self.gpioG, self.G)
            self.pi.set_PWM_dutycycle(self.gpioB, self.B)

        return self.currentState
        
    def IsHWOK(self):
        return True

    def SwitchOn(self, on, r = 0, g = 0, b = 0):
        self.currentState = on
        if on == 0:
            self.pi.set_PWM_dutycycle(self.gpioR, 0)
            self.pi.set_PWM_dutycycle(self.gpioG, 0)
            self.pi.set_PWM_dutycycle(self.gpioB, 0)
        elif on == 1:   # Automatic mode
            self.autoState = 0
            self.R = 255
            self.G = 0
            self.B = 0
        else:            # Manual mode
            self.pi.set_PWM_dutycycle(self.gpioR, r)
            self.pi.set_PWM_dutycycle(self.gpioG, g)
            self.pi.set_PWM_dutycycle(self.gpioB, b)
            self.R = r;
            self.G = g;
            self.B = b;

    def GetState(self):
        return self.currentState

if __name__ == "__main__":

   pi = pigpio.pi()
   s = ledRgbDrv(pi, 7, 8, 9)

