# -*- coding: cp1250 -*-
import texts

class hwDriverGeneralClass:
    """General class for hardware drivers"""
    def __init__(self):
        self.ErrorMessage = texts.text_nondefined_error

    def OnTick(self):
        pass
        
    def IsHWOK(self):
        pass
        
    def GetFaultMessage(self):
        return self.ErrorMessage
