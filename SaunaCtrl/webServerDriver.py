import os
import tempfile
import pygame
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from os import curdir, sep
import cgi
import threading
import time
import hwDriverGeneralClass
import platform

PORT_NUMBER = 8080

MainSurface = None

class webServerDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for built-in web server"""
    def __init__(self, mainSurface):
        global MainSurface
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        MainSurface = mainSurface
        # Start Server
        self.webserver = HTTPServer(('', PORT_NUMBER), reqHandlerClass)
        self.webserver_thread = threading.Thread( target = self.run_webserver_thread)
        self.webserver_thread.start()
        
    def OnTick(self):
        return 0

    def IsHWOK(self):
        return True

    def StopServer(self):
        self.webserver.shutdown()

    def run_webserver_thread(self):
        try:
            self.webserver.serve_forever();
        except KeyboardInterrupt:
            self.webserver.socket.close()

class reqHandlerClass(BaseHTTPRequestHandler):   
    #Handler for the GET requests
    def do_GET(self):
	if self.path=="/":
		self.path="./Web/index.html"
        try:
            #Check the file extension required and
            #set the right mime type
            print ("Uploading:" + self.path)
            if '?'in self.path:
            	self.path = self.path[:self.path.index('?')]
                sendReply = False
            if self.path.endswith(".html"):
            	mimetype='text/html'
            	binary = False
            	sendReply = True
            if self.path.endswith(".bmp"):
            	mimetype='image/bmp'
            	binary = True
            	sendReply = True
	        if self.path.endswith(".gif"):
		        mimetype= 'image/gif'
		        binary = True
		        sendReply = True
            if self.path.endswith(".jpg"):
                mimetype='image/jpg'
                binary = True
                if self.path.endswith("virtual.jpg"):
                    # Send mimetype
                    self.send_response(200)
                    self.send_header('Content-type',mimetype)
                    self.end_headers()
                    if platform.system() != 'Windows':
                        # http://www.domoticz.com/wiki/Setting_up_a_RAM_drive_on_Raspberry_Pi
                        pygame.image.save(MainSurface, "/var/tmp/virtual.jpg")
                        f = open("/var/tmp/virtual.jpg", "rb")
                        self.wfile.write(f.read())
                        f.close()
                    else:
                        # Windows
                        pygame.image.save(MainSurface, "./virtual.jpg")
                        f = open("./virtual.jpg", "rb")
                        self.wfile.write(f.read())
                        f.close()
                    # Reply already sent.
                    sendReply = False
                else:
                    # Real File.
                    sendReply = True		
            if self.path.endswith(".js"):
		        mimetype='application/javascript'
		        binary = False
		        sendReply = True
            if self.path.endswith(".css"):
                mimetype='text/css'
                binary = False
                sendReply = True

            if sendReply == True:
                #Open the static file requested and send it
                if binary:
                    f = open(curdir + sep + self.path, "rb")
                else:
                    f = open(curdir + sep + self.path, "r")
            
            if mimetype != None:
                self.send_response(200)
                self.send_header('Content-type',mimetype)
                self.end_headers()
                
                if sendReply == True:
                    self.wfile.write(f.read())
                f.close()
                    
	        return
        
        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)

    #Handler for the POST requests
    def do_POST(self):
	print("Post:" + self.path)
	form = cgi.FieldStorage(
	fp=self.rfile,headers=self.headers, environ={'REQUEST_METHOD':'POST', 'CONTENT_TYPE':self.headers['Content-Type'],})
	pygame.mouse.set_pos(int(form["form_x"].value) , int(form["form_y"].value))
	pygame.event.post(pygame.event.Event(pygame.MOUSEBUTTONDOWN, pos =(int(form["form_x"].value) , int(form["form_y"].value)), button = 1))
	pygame.event.post(pygame.event.Event(pygame.MOUSEBUTTONUP, pos =(int(form["form_x"].value) , int(form["form_y"].value)), button = 1))

	self.send_response(200)
	self.end_headers()
	f = open("./Web/index.html", "r")
	self.wfile.write(f.read())
	f.close()
	return
