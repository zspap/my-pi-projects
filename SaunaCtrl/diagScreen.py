# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import texts


class diagScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver

        self.R = 255
        self.G = 255
        self.B = 255
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 25)
        self.buttonCancel = pygbutton.PygButton((0, 0, 53, 50),  texts.text_back , bgcolor=(0,0,255), fgcolor= (255,255,255))
        self.buttonVent0 = pygbutton.PygButton((53, 0, 53, 50),  texts.text_vent0, bgcolor=(255,0,0), fgcolor= (255,255,255))
        self.buttonVent1 = pygbutton.PygButton((106, 0, 53, 50), texts.text_vent1, bgcolor=(0,255,0), fgcolor= (255,255,255))
        self.buttonLamp0 = pygbutton.PygButton((159, 0, 53, 50), texts.text_lmp0, bgcolor=(255,0,0), fgcolor= (255,255,255))
        self.buttonLamp1 = pygbutton.PygButton((212, 0, 53, 50), texts.text_lmp1, bgcolor=(0,255,0), fgcolor= (255,255,255))
        self.buttonExit = pygbutton.PygButton((265, 0, 53, 50),  texts.text_exit, bgcolor=(0,0,0), fgcolor= (255,255,255))

        self.buttonRGB0 =  pygbutton.PygButton((0, 50, 53, 50), texts.text_rgb0, bgcolor=(250,0,0), fgcolor= (255,255,255))
        self.buttonRGB1 =  pygbutton.PygButton((53, 50, 53, 50), texts.text_rgb1, bgcolor=(255,255,0), fgcolor= (255,255,255))
        self.buttonRGB2 =  pygbutton.PygButton((106, 50, 53, 50), texts.text_rgb2, bgcolor=(0,255,255), fgcolor= (255,255,255))
        self.buttonR =  pygbutton.PygButton((159, 50, 53, 50), texts.text_rplusplus, bgcolor=(255,0,0), fgcolor= (255,255,255))
        self.buttonG =  pygbutton.PygButton((212, 50, 53, 50), texts.text_gplusplus, bgcolor=(0,255,0), fgcolor= (255,255,255))
        self.buttonB =  pygbutton.PygButton((265, 50, 53, 50), texts.text_bplusplus, bgcolor=(0,0,255), fgcolor= (255,255,255))

        self.buttonInfra0 = pygbutton.PygButton((0, 100, 53, 50),  texts.text_Inf0, bgcolor=(255,0,0), fgcolor= (255,255,255))
        self.buttonInfra1 = pygbutton.PygButton((53, 100, 53, 50), texts.text_Inf1, bgcolor=(0,255,0), fgcolor= (255,255,255))
        self.buttonHeat0 = pygbutton.PygButton((106, 100, 53, 50), texts.text_heating0, bgcolor=(255,32,0), fgcolor= (255,255,255))
        self.buttonHeat1 = pygbutton.PygButton((159, 100, 53, 50), texts.text_heating1, bgcolor=(0,64,0), fgcolor= (255,255,255))
        self.buttonHeat2 = pygbutton.PygButton((212, 100, 53, 50), texts.text_heating2, bgcolor=(0,128,0), fgcolor= (255,255,255))
        self.buttonHeat3 = pygbutton.PygButton((265, 100, 53, 50), texts.text_heating3, bgcolor=(0,255,0), fgcolor= (255,255,255))

        self.buttonPump0 = pygbutton.PygButton((0, 150, 53, 50), texts.text_pump0, bgcolor=(255,0,0), fgcolor= (255,255,255))
        self.buttonPump1 = pygbutton.PygButton((53, 150, 53, 50), texts.text_pump1, bgcolor=(0,255,0), fgcolor= (255,255,255))
        self.buttonMusic0 = pygbutton.PygButton((159, 150, 53, 50), texts.text_musicOFF, bgcolor=(255,0,0), fgcolor= (255,255,255))
        self.buttonMusic1 = pygbutton.PygButton((106, 150, 53, 50), texts.text_musicON, bgcolor=(0,255,0), fgcolor= (255,255,255))

    def OnDraw(self, surface):
        # Draw buttns
        self.buttonCancel.draw(surface)
        self.buttonVent0.draw(surface)
        self.buttonVent1.draw(surface)
        self.buttonLamp0.draw(surface)
        self.buttonLamp1.draw(surface)
        self.buttonExit.draw(surface)
        self.buttonRGB0.draw(surface)
        self.buttonRGB1.draw(surface)
        self.buttonRGB2.draw(surface)
        self.buttonR.draw(surface)
        self.buttonG.draw(surface)
        self.buttonB.draw(surface)
        self.buttonInfra0.draw(surface)
        self.buttonInfra1.draw(surface)
        self.buttonHeat0.draw(surface)
        self.buttonHeat1.draw(surface)
        self.buttonHeat2.draw(surface)
        self.buttonHeat3.draw(surface)
        self.buttonPump0.draw(surface)
        self.buttonPump1.draw(surface)
        self.buttonMusic0.draw(surface)
        self.buttonMusic1.draw(surface)
        # Draw States
        temp2 = self.systemDriver.drivers[systemDriver.DRV_TEMPSENS1].temperature()
        hum2 = self.systemDriver.drivers[systemDriver.DRV_TEMPSENS1].humidity()
        # Draw Temp and Hum.
        color = (255, 255, 255)

        if temp2 >= 100:
            temp_text = "{:.0f}".format(temp2) + texts.text_celsius1 + "{:.0f}".format(hum2) + "%"
        else:
            temp_text = "{:.1f}".format(temp2) + texts.text_celsius1 + "{:.0f}".format(hum2) + "%"
        label = self.font0.render(temp_text, 1, color)
        surface.blit(label, (160, 200))

        temp_text = "A:" + str(self.systemDriver.drivers[systemDriver.DRV_DOOR].GetState())
        label = self.font0.render(temp_text, 1, color)
        surface.blit(label, (0, 220))

        temp_text = "W:" + str(self.systemDriver.drivers[systemDriver.DRV_PUMP].GetWaterLevel())
        label = self.font0.render(temp_text, 1, color)
        surface.blit(label, (40, 220))

        temp_text =  texts.text_diag_V + str(self.systemDriver.drivers[systemDriver.DRV_VENT].GetState())
        temp_text += texts.text_diag_L + str(self.systemDriver.drivers[systemDriver.DRV_LAMP].GetState())
        temp_text += texts.text_diag_F + str(self.systemDriver.drivers[systemDriver.DRV_RGB].GetState())
        temp_text += texts.text_diag_P + str(self.systemDriver.drivers[systemDriver.DRV_PUMP].GetState())
        temp_text += texts.text_diag_H + str(self.systemDriver.drivers[systemDriver.DRV_HEATER].GetState())
        temp_text += texts.text_diag_I + str(self.systemDriver.drivers[systemDriver.DRV_INFRA].GetState())
        label = self.font0.render(temp_text, 1, color)
        surface.blit(label, (120, 220))

    def OnEvent(self, event):
        if 'click' in self.buttonCancel.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_SAUNA].SwitchOff()
            self.systemDriver.drivers[systemDriver.DRV_INFRA].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_HEATER].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_SOUND].Play(0)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(0)
            self.systemDriver.drivers[systemDriver.DRV_PUMP].SwitchOn(0)            
            return graphDriver.SC_DRIVER_OFF
        if 'click' in self.buttonVent0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(0, 0)
            return 0
        if 'click' in self.buttonVent1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_VENT].SwitchOn(1, 0)
            return 0
        if 'click' in self.buttonLamp0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(0, 0)
            return 0
        if 'click' in self.buttonLamp1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(1, 0)
            return 0
        if 'click' in self.buttonPump0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_PUMP].SwitchOn(-1)
            return 0
        if 'click' in self.buttonPump1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_PUMP].SwitchOn(2)
            return 0
        if 'click' in self.buttonInfra0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_INFRA].SwitchOn(0)
            return 0
        if 'click' in self.buttonInfra1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_INFRA].SwitchOn(1)
            return 0
        if 'click' in self.buttonHeat0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_HEATER].SwitchOn(0)
            return 0
        if 'click' in self.buttonHeat1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_HEATER].SwitchOn(1)
            return 0
        if 'click' in self.buttonHeat2.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_HEATER].SwitchOn(2)
            return 0
        if 'click' in self.buttonHeat3.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_HEATER].SwitchOn(3)
            return 0
        if 'click' in self.buttonRGB0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(0, self.R, self.G, self.B)
            return 0
        if 'click' in self.buttonRGB1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(1, self.R, self.G, self.B)
            return 0
        if 'click' in self.buttonRGB2.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(2, self.R, self.G, self.B)
            return 0

        if 'click' in self.buttonR.handleEvent(event):
            self.R += 31
            if self.R > 255:
                self.R = 0
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(2, self.R, self.G, self.B)
            return 0
        if 'click' in self.buttonG.handleEvent(event):
            self.G += 31
            if self.G > 255:
                self.G = 0
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(2, self.R, self.G, self.B)
            return 0
        if 'click' in self.buttonB.handleEvent(event):
            self.B += 31
            if self.B > 255:
                self.B = 0
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(2, self.R, self.G, self.B)
            return 0
        if 'click' in self.buttonMusic0.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_SOUND].SetVolume(50)
            self.systemDriver.drivers[systemDriver.DRV_SOUND].Play(0)
            return 0

        if 'click' in self.buttonMusic1.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_SOUND].SetVolume(50)
            self.systemDriver.drivers[systemDriver.DRV_SOUND].Play(1, texts.path_Kitaro, 1)
            return 0
  
        if 'click' in self.buttonExit.handleEvent(event):
            self.systemDriver.drivers[systemDriver.DRV_SAUNA].StartSauna(saunaControl.MODE_OFF)
            return -2 
        return 0
