import os
import time


simuTemperature = 22.0
simuHumidity = 56
simuHeater = 0
simuInfra = 0
simuVent = 0
simuLamp = 0
simuPump = 0
simuDoor = 0
simuRGB = 0
simuLevel = 0
simuWDT = 0
simuCounter = 0


def SimuStep():
    global simuTemperature 
    global simuHumidity 
    global simuHeater
    global simuInfra
    global simuVent
    global simuLamp
    global simuPump
    global simuDoor
    global simuRGB
    global simuLevel
    global simuWDT
    global simuCounter

    if simuWDT:
    
        if simuHeater == 1:
            simuTemperature = simuTemperature + 0.1
        
        if simuHeater == 2:
            simuTemperature = simuTemperature + 0.2

        if simuHeater == 3:
            simuTemperature = simuTemperature + 0.3

        if simuInfra:
            simuTemperature = simuTemperature + 0.1

        if simuVent:
            simuTemperature = simuTemperature - 0.1

        if simuDoor:
            simuTemperature = simuTemperature - 0.3

    simuTemperature = simuTemperature * 0.999

    if simuTemperature < 18:
        simuTemperature = 18

    string = "Temp:" + str(round(simuTemperature,2)) + "C  "
    if simuHeater:
        string = string + str(simuHeater) + " "
    else:
        string = string + " "

    if simuWDT:
        string = string + "WDT "
    else:
        string = string + "    "

    if simuInfra:
        string = string + "I "
    else:
        string = string + "  "
        
    if simuVent:
        string = string + "V "
    else:
        string = string + "  "
  
    if simuLamp:
        string = string + "L "
    else:
        string = string + "  "

    if simuRGB:
        string = string + "RGB "
    else:
        string = string + "    "

    if simuPump:
        string = string + "P "
    else:
        string = string + "  "
  
    if simuDoor:
        string = string + "D "
    else:
        string = string + "  "
  
    if simuLevel:
        string = string + "W "
    else:
        string = string + "  "

    if simuWDT:
        simuCounter += 1
        #if  not (simuCounter & 64):
        #    simuDoor = 1
        #else:
        #    simuDoor = 0
        print(string)
  
