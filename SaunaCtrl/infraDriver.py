# -*- coding: cp1250 -*-
import time
import pigpio
import hwDriverGeneralClass
import texts

class infraDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for infra emitters"""
    def __init__(self, pi, gpio):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.safetyTimer = 0
        self.pi = pi
        self.gpio = gpio
        self.pi.set_mode(gpio, pigpio.OUTPUT)
        self.currentState = 0    # 0: OFF.  1: ON

        self.OperationTime = 0

    def OnTick(self):
        self.safetyTimer = self.safetyTimer + 1
        if not self.IsHWOK():         # Force off infra.
            self.pi.write(self.gpio, 0)
            self.pi.write(self.gpio, 0)
        elif self.currentState > 0:
            self.OperationTime = self.OperationTime + 1;

        return self.currentState
        
    def IsHWOK(self):
        if (self.currentState > 0) and (self.safetyTimer > (8 * 3600)):
            self.ErrorMessage = texts.text_infraerror
            return False
        return True

    def SwitchOn(self, on):
        if self.currentState != on:
           self.safetyTimer = 0
        self.currentState = on
        if self.currentState == 0:
            self.pi.write(self.gpio, 0)
        else:
            self.pi.write(self.gpio, 1)

    def GetState(self):
        return self.currentState

    def GetSetOperationTime(self, clear):
        if clear:
            self.OperationTime = 0    
        return self.OperationTime

