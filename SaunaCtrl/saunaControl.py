
# -*- coding: cp1250 -*-
import time
import hwDriverGeneralClass
import pygame
import heaterDriver
import infraDriver
import ventDriver
import tempSensorDrv
import texts        
import logger

CONSUMPTION_HEATER = 4600.0      # 4600W on 3-phase 
CONSUMPTION_INFRA = 350.0 * 4.0  # 350W * 4
PRICE_OF_POWER = 45.0            # Ft/kWh


MODE_OFF      = 0
MODE_FAULT    = 1
MODE_INFRA    = 2
MODE_HEAT     = 3
MODE_CLEANING = 4
MODE_CSILLA   = 5
MODE_DIAG     = 6

HYSTERESIS    = 1.0

DUALMODE_INFRATIME = 600    # 600 = 5 minutes

class saunaControl(hwDriverGeneralClass.hwDriverGeneralClass):
    """ General Logics for Sauna"""
    def __init__(self, hDrv, iDrv, vDrv, t1Drv, doorDrv):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.saunaMode = MODE_OFF
        self.temperature = 0
        self.timeToStop = 0
        self.hDrv = hDrv
        self.iDrv = iDrv
        self.t1Drv = t1Drv
        self.vDrv = vDrv
        self.hysteresis = -HYSTERESIS
        self.combinedTemp = 199
        self.combinedhumidity = 99
        self.heaterState = texts.text_setupstate
        self.timeToGo = 0
        self.tempDiff = 0
        self.ventmode = 0
        self.originalTemp = -999
        self.estimatingConstant = 50   # Seconds to increase one degree.
        self.heatingUpSeconds = 0.0
        self.dualmodecounter = 0
        self.doorDriver = doorDrv
        
    def OnTick(self):
        self.ComputeState()
        if self.timeToGo > 0:
            self.timeToGo -= 1
            if self.timeToGo == 0:
                # Switch off sauna
                logger.MainLogger.LogEvent("Normal Timeout. Stop Sauna.")
                self.SwitchOff()

        self.heatingUpSeconds += 0.5
        self.dualmodecounter += 1

        #If door is open, dualmode counter is cleared.
        if self.doorDriver.GetState() > 0:
            self.dualmodecounter = 0
       
        return self.saunaMode
        
    def IsHWOK(self):
        if self.saunaMode == MODE_FAULT:
            return False
        return True

    def ComputeState(self):
        if self.saunaMode == MODE_OFF:
            self.hDrv.SwitchOn(0)        # Off
            self.iDrv.SwitchOn(0)        # Off
            self.heaterState = texts.text_heateroff
            self.comTemperature(self.t1Drv)
        elif self.saunaMode == MODE_INFRA:
            # Infra mode 
            temp = self.comTemperature(self.t1Drv)
            temp = temp + self.hysteresis;
            if temp < (self.temperature - 20):
                # IF too cold, Infra mode must not be activated. Heat up first.
                self.hDrv.SwitchOn(3)        # On
                self.iDrv.SwitchOn(0)        # Off
                self.hysteresis = -HYSTERESIS
                self.heaterState = texts.text_heatup + ':' + self.GetPreheatingPercent(26)
                self.tempDiff = -2
                self.ventmode = 1
            elif temp < self.temperature:
                if self.ventmode != 2:
                    self.vDrv.SwitchOn(0)    # Off
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(1)        # On
                self.hysteresis = -HYSTERESIS
                self.heaterState = texts.text_inframode
                self.tempDiff = -1
                self.ventmode = 2
            elif temp > 130:
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.vDrv.SwitchOn(0)        # Off
                self.saunaMode = MODE_FAULT
                self.hysteresis = HYSTERESIS
                self.heaterState = texts.text_overheating
                self.ErrorMessage = texts.text_overheating
                self.tempDiff = 3
                self.ventmode = 3
            elif temp > (self.temperature + 5):
                if self.ventmode != 4:
                    self.vDrv.SwitchOn(1)    # On
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.hysteresis = HYSTERESIS
                self.heaterState = texts.text_inframode
                self.tempDiff = 2
                self.ventmode = 4
            else:
                # Between   self.temperature   and   self.temperature+5
                if self.ventmode != 5:
                    self.vDrv.SwitchOn(1)    # On
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(1)        # On
                # Hysteresis remains the same.
                self.heaterState = texts.text_inframode
                self.tempDiff = 0
                self.ventmode = 5
        elif self.saunaMode == MODE_HEAT:
            # Finnish (heat) mode
            temp = self.comTemperature(self.t1Drv)
            temp = temp + self.hysteresis;
            if temp < self.temperature:
                self.hDrv.SwitchOn(3)        # On
                self.iDrv.SwitchOn(0)        # Off
                self.hysteresis = -HYSTERESIS
                if temp < self.temperature - 5:
                    self.heaterState = texts.text_heatup+ ':' + self.GetPreheatingPercent(self.temperature - 5)
                    self.tempDiff = -2
                else:
                    self.heaterState = texts.text_finnishmode
                    self.tempDiff = -1
                self.ventmode = 1                
            elif temp > 130:
                if self.ventmode != 3:
                    self.vDrv.SwitchOn(0)    # Off
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.saunaMode = MODE_FAULT
                self.heaterState = texts.text_overheating
                self.ErrorMessage = texts.text_overheating
                self.hysteresis = HYSTERESIS
                self.tempDiff = 3
                self.ventmode = 2
            elif temp > (self.temperature + 10):
                if self.ventmode != 3:
                    self.vDrv.SwitchOn(1)    # On
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.heaterState = texts.text_finnishmode
                self.hysteresis = HYSTERESIS
                self.tempDiff = 2
                self.ventmode = 3
            else:
                if self.ventmode != 4:
                    self.vDrv.SwitchOn(0)    # Off
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.heaterState = texts.text_finnishmode
                self.hysteresis = HYSTERESIS
                self.tempDiff = 0
                self.ventmode = 4
        elif self.saunaMode == MODE_CSILLA:
            # Csilla-mode 
            temp = self.comTemperature(self.t1Drv)
            temp = temp + self.hysteresis;
            if temp < self.temperature:
                if self.dualmodecounter > DUALMODE_INFRATIME:
                    self.hDrv.SwitchOn(2)        # On/Half
                    self.iDrv.SwitchOn(1)        # On
                    if self.dualmodecounter > (DUALMODE_INFRATIME * 2):
                        self.dualmodecounter = 0
                else:
                    self.hDrv.SwitchOn(3)        # On
                    self.iDrv.SwitchOn(0)        # Off                self.hysteresis = -HYSTERESIS
                if temp < self.temperature - 5:
                    self.heaterState = texts.text_heatup+ ':' + self.GetPreheatingPercent(self.temperature - 5)
                    self.tempDiff = -2
                else:
                    self.heaterState = texts.text_finnishmode
                    self.tempDiff = -1
                self.ventmode = 1                
            elif temp > 130:
                if self.ventmode != 3:
                    self.vDrv.SwitchOn(0)    # Off
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.saunaMode = MODE_FAULT
                self.heaterState = texts.text_overheating
                self.ErrorMessage = texts.text_overheating
                self.hysteresis = HYSTERESIS
                self.tempDiff = 3
                self.ventmode = 2
            elif temp > (self.temperature + 10):
                if self.ventmode != 3:
                    self.vDrv.SwitchOn(1)    # On
                self.hDrv.SwitchOn(0)        # Off
                self.iDrv.SwitchOn(0)        # Off
                self.heaterState = texts.text_dualmode
                self.hysteresis = HYSTERESIS
                self.tempDiff = 2
                self.ventmode = 3
            else:
                if self.ventmode != 4:
                    self.vDrv.SwitchOn(0)    # Off
                self.hDrv.SwitchOn(0)        # Off
                if self.dualmodecounter > DUALMODE_INFRATIME:
                    self.iDrv.SwitchOn(1)        # On
                    if self.dualmodecounter > (DUALMODE_INFRATIME * 2):
                        self.dualmodecounter = 0
                else:
                    self.iDrv.SwitchOn(0)        # Off
                self.heaterState = texts.text_dualmode
                self.hysteresis = HYSTERESIS
                self.tempDiff = 0
                self.ventmode = 4
        elif self.saunaMode == MODE_CLEANING:
            # Cleaning mode
            self.hDrv.SwitchOn(0)        # Off
            self.iDrv.SwitchOn(0)        # Off
            #self.vDrv.SwitchOn(1)        # On
            self.heaterState = texts.text_cleaningmode
            self.tempDiff = 0
            self.comTemperature(self.t1Drv)
        elif self.saunaMode == MODE_DIAG:
            # Diagnostics mode 
            # Do nothing
            self.heaterState = texts.text_diagmode            
        else:    # Fault
            self.hDrv.SwitchOn(0)        # Off
            self.iDrv.SwitchOn(0)        # Off
            self.vDrv.SwitchOn(0)        # Off
            self.heaterState = texts.text_errormode
            self.tempDiff = 0
            self.comTemperature(self.t1Drv)

    def comTemperature(self, sensor2):
        temp2 = sensor2.temperature()
        fault2 = sensor2.IsHWOK()

        if not fault2:
           self.saunaMode = MODE_FAULT
           self.ErrorMessage = texts.text_sensorfauilure
           return

        self.combinedTemp = temp2
        self.combinedhumidity = sensor2.humidity()

        return self.combinedTemp 

    def GetTemperature(self):
        return self.combinedTemp

    def GetRefTemperature(self):
        return self.temperature

    def GetHumidity(self):
        return self.combinedhumidity

    def GetStatTxt(self):
        return self.heaterState

    def GetTimeToGo(self):
        return self.timeToGo / 2

    def SwitchOff(self):
        self.saunaMode = MODE_OFF
        self.heaterState = texts.text_offmode

    def GetSaunaMode(self):
        if self.saunaMode == MODE_CLEANING:
            self.vDrv.SwitchOn(1)        # On
        return self.saunaMode

    def SetSaunaMode(self, mode):
        self.saunaMode = mode

    def StartSauna(self, mode = MODE_OFF, temperature = 70, timetogo = 10800):
        self.timeToGo = timetogo * 2
        self.saunaMode = mode
        self.temperature = temperature
        self.heatingUpSeconds = 0.0
        self.originalTemp = self.comTemperature(self.t1Drv)
        logger.MainLogger.LogEvent("Set Operation Mode to " + str(mode))

    def GetTempDiff(self):
        return self.tempDiff

    def SetTemperature(self, temp):
        self.temperature = temp

    def SetTimeToGo(self, time):
        self.timeToGo = time * 2

    def GetPreheatingPercent(self, destinationTemp):
        total = destinationTemp - self.originalTemp
        diff1 = self.combinedTemp - self.originalTemp
        diff2 = destinationTemp - self.combinedTemp
        time = diff2 * self.estimatingConstant
        # Calculate percent
        perCent = (diff1 * 100) / total
        #print("Time:" + str(time))
        #print("total:" +str(total))
        #print("diff1:" +str(diff2))
        #print("diff2:" +str(diff1))
        #print("PerCent:" +str(perCent))
        #print("self.originalTemp:" +str(self.originalTemp))
        #print("destinationTemp:" +str(destinationTemp))
        #print("self.estimatingConstant:" +str(self.estimatingConstant))
        if perCent < 0:
            perCent = 0
        if perCent > 100:
            perCent = 100
        # Do correction of hetup estimation speed.
        if (self.heatingUpSeconds > 20) and (diff1 > 5):
            heatupspeed = self.heatingUpSeconds/diff1   # Seconds/Celsius
            if heatupspeed > 0:
                self.estimatingConstant = (self.estimatingConstant + heatupspeed) / 2.0
        # Generate text
        return "{:.0f}".format(perCent) + '% ' + "{:.0f}".format(time / 60) + texts.text_minutes
       
    def GetConsumption(self):
        hoptime = self.hDrv.GetSetOperationTime(False)
        ioptime = self.iDrv.GetSetOperationTime(False)
        cons = hoptime * (CONSUMPTION_HEATER / 6.0)    # 1-phase WattSeconds
        cons += (ioptime * (CONSUMPTION_INFRA / 2.0))  # 1-phase WattSeconds
        return ((cons / 3600.0) / 1000.0)              # In Ws -> kWh

    def GetMoney(self):
        return self.GetConsumption() * PRICE_OF_POWER
        
    def ClearConsumption(self):
        self.hDrv.GetSetOperationTime(True)
        self.iDrv.GetSetOperationTime(True)
