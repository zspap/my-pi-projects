import time
import pigpio
import os
import pygame, pygbutton, sys
from pygame.locals import *
from subprocess import call
import hwDriverGeneralClass
import locale
import mainWdtDriver
import pumpDriver
import lampDriver
import ventDriver
import tempSensorDrv
import heaterDriver
import infraDriver
import ledRgbDriver
import soundDriver
import saunaControl
import doorCheckDriver
import graphDriver
import platform
import webServerDriver
import logger
import traceback


GPIO_PWM_B   = 2
GPIO_PWM_G   = 3
GPIO_PWM_R   = 4
GPIO_INFRA   = 14
GPIO_WDT     = 15
GPIO_HEATERL = 17
GPIO_PUMP    = 18
GPIO_LAMP    = 22 
GPIO_VENT    = 23
GPIO_HEATERH = 27
GPIO_SENSOR1 = 28   # Sensor Power Control
GPIO_SENSOR2 = 29   # Sensor data channel
GPIO_WTR_LVL = 30
GPIO_DOOR    = 31
GPIO_SPARE   = 31   # same as DOOR

DRV_LOGGER    = 0
DRV_PUMP      = 1
DRV_SAUNA     = 2
DRV_DISPLAY   = 3
DRV_HEATER    = 4
DRV_INFRA     = 5
DRV_LAMP      = 6
DRV_VENT      = 7
DRV_RGB       = 8
DRV_WDT       = 9
DRV_SOUND     = 10
DRV_DOOR      = 11
DRV_TEMPSENS1 = 12
DRV_WEBSERVER = 13


class MainSystemDriver():
    """Main System driver handling loop class"""
    def __init__(self):
        pygame.init()
        if platform.system() != 'Windows':
            pygame.mouse.set_visible(False)

        self.pi = pigpio.pi()

        self.drivers = [None] * 14
        self.drivers[DRV_LOGGER] = logger.loggerDriver()
        self.drivers[DRV_PUMP] = pumpDriver.pumpDrv(self.pi, GPIO_PUMP, GPIO_WTR_LVL)
        self.drivers[DRV_TEMPSENS1] = tempSensorDrv.tempSensorDrv(self.pi, GPIO_SENSOR2, GPIO_SENSOR1)
        self.drivers[DRV_HEATER] = heaterDriver.heaterDrv(self.pi, GPIO_HEATERL, GPIO_HEATERH)
        self.drivers[DRV_INFRA] = infraDriver.infraDrv(self.pi, GPIO_INFRA)
        self.drivers[DRV_LAMP] = lampDriver.lampDrv(self.pi, GPIO_LAMP)
        self.drivers[DRV_VENT] = ventDriver.ventDrv(self.pi, GPIO_VENT)
        self.drivers[DRV_RGB] = ledRgbDriver.ledRgbDrv(self.pi, GPIO_PWM_R,GPIO_PWM_G,GPIO_PWM_B)
        self.drivers[DRV_DOOR] = doorCheckDriver.doorCheckDrv(self.pi, GPIO_DOOR)
        self.drivers[DRV_SAUNA] = saunaControl.saunaControl(self.drivers[DRV_HEATER], self.drivers[DRV_INFRA], self.drivers[DRV_VENT], self.drivers[DRV_TEMPSENS1], self.drivers[DRV_DOOR])
        self.drivers[DRV_WDT] = mainWdtDriver.systemWdtDrv(self.pi, GPIO_WDT)
        self.drivers[DRV_SOUND] = soundDriver.soundDrv()
        self.drivers[DRV_DISPLAY] = graphDriver.GraphDriver(self)
        self.drivers[DRV_WEBSERVER] = webServerDriver.webServerDrv(self.drivers[DRV_DISPLAY].GetMainSurface())
        self.StopOperation = False
            
    def Destroy(self):
        self.StopOperation = True
        self.drivers[DRV_TEMPSENS1].cancel()
        self.drivers[DRV_DISPLAY].Destroy()
        self.drivers[DRV_SOUND].Play(0)
        self.drivers[DRV_WEBSERVER].StopServer() 
        self.drivers[DRV_LOGGER].CloseLog()
        
    def MainHwDriverLoop(self):
        tick = self.pi.get_current_tick()
        while not self.StopOperation:

            useWDT = False
            lastDriver= None;
            tick2 = self.pi.get_current_tick() - tick
            if (tick2 > 500000) or (tick2 < 0):
                tick = self.pi.get_current_tick()
                try:
                    _errorCode = None;
                    for driver in self.drivers:
                        if driver:
                            lastDriver= driver
                            if driver.OnTick() > 0:
                                useWDT = True
                            if not driver.IsHWOK():
                                print("Driver problem:" + str(driver))
                                _errorCode = driver;
                                break
                except:
                    print("Driver Exception:" + str(lastDriver))
                self.errorCode = _errorCode
                if self.errorCode != None:
                    # Stop the system
                    errorMessage = self.errorCode.GetFaultMessage()
                    self.drivers[DRV_SAUNA].SwitchOff()
                    self.drivers[DRV_SOUND].Play(0)
                    self.drivers[DRV_RGB].SwitchOn(0)
                    self.drivers[DRV_LAMP].SwitchOn(0)
                    self.drivers[DRV_VENT].SwitchOn(0)
                    self.drivers[DRV_PUMP].SwitchOn(0)
                    self.drivers[DRV_INFRA].SwitchOn(0)
                    self.drivers[DRV_HEATER].SwitchOn(0)
                    self.drivers[DRV_WDT].SwitchOn(0)
                    useWDT = False;
                    # Activate Error Screen
                    self.drivers[DRV_DISPLAY].WriteFault(errorMessage)

                if useWDT:
                    self.drivers[DRV_WDT].SwitchOn(1)
                else:
                    self.drivers[DRV_WDT].SwitchOn(0)

            ret = self.drivers[DRV_DISPLAY].MainScreenLoopStep()
            if ret == -1:      # Exit command 
                self.StopOperation = 1
                break

    def GetFaultyDriver(self):
        return self.errorCode


if __name__ == "__main__":

    if platform.system() != 'Windows':
        os.environ["SDL_VIDEODRIVER"] = "fbcon"
        os.environ["SDL_FBDEV"] = "/dev/fb1"
        os.environ["SDL_MOUSEDRV"] = "TSLIB"
        os.environ["SDL_MOUSEDEV"] = "/dev/input/touchscreen"
        os.system("echo 252 > /sys/class/gpio/export")
        os.system("echo 'out' > /sys/class/gpio/gpio252/direction")
        os.system("./pigpiod")
        time.sleep(1)

    hwDrv = MainSystemDriver()

    try:
        hwDrv.MainHwDriverLoop()
    except Exception, e:
        print("System Main Exception")   
        if logger.MainLogger:
            logger.MainLogger.LogEvent("Critical system failure. Program Stop. ::" + str(e) + "::")
            traceback.print_exc(file = logger.MainLogger.logFile)
        hwDrv.Destroy()
        pygame.quit()
        raise
    
    hwDrv.Destroy()
    pygame.quit()
    sys.exit()
    
