import time
import pigpio
import hwDriverGeneralClass

class systemWdtDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """System WDT Timer driver """
    def __init__(self, pi, gpio):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.pi = pi
        self.gpio = gpio
        self.pi.set_mode(gpio, pigpio.OUTPUT)
        self.pi.set_pull_up_down(gpio, pigpio.PUD_OFF)
        self.currentState = 0    # 0: OFF.  1: ON
        self.currentPulseState = 0

    def OnTick(self):
        if self.currentState > 0:
            if self.currentPulseState == 0:
                self.currentPulseState = 1
                self.pi.write(self.gpio, 1)
            else:
                self.currentPulseState = 0
                self.pi.write(self.gpio, 0)
        else:
            self.currentPulseState = 0
            self.pi.write(self.gpio, 0)
        return 0
    
    def IsHWOK(self):
        return True

    def SwitchOn(self, on):
        self.currentState = on
        if on != 0:
            self.currentPulseState = 1
            self.pi.write(self.gpio, 1)
        else:
            self.currentPulseState = 0
            self.pi.write(self.gpio, 0)

