# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import platform
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import texts 


monthNames = ['', texts.text_jan, texts.text_febr, texts.text_mar, texts.text_apr, texts.text_may ,texts.text_jun ,texts.text_jul, texts.text_aug, texts.text_sept, texts.text_oct ,texts.text_nov, texts.text_dec]

class offScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        self.backlightTimer = 1600
        self.doorDrv = sysDriver.drivers[systemDriver.DRV_DOOR]
        self.doorstate = False
        self.lightDrv = sysDriver.drivers[systemDriver.DRV_LAMP]
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 30)
        self.font1 = pygame.font.Font(None, 40)
        self.font2 = pygame.font.Font(None, 100)
        self.font3 = pygame.font.Font(None, 60)
        self.background = pygame.image.load('./artwork/backgroundOff3.bmp')
        
        self.currentBacklight = -1
        
    def OnDraw(self, surface):
        # Background image
        surface.blit(self.background, (0, 0))

        # Handle backlight
        if self.backlightTimer > 0:
            self.backlightTimer -= 1
            self.SetBacklight(1)
        else:
            self.SetBacklight(0)

        # Draw Clock
        clock_text = datetime.datetime.now().strftime("%Y - ") + monthNames[datetime.datetime.now().month] + datetime.datetime.now().strftime(" - %d")
        label = self.font1.render(clock_text, 1, (255,255, 255))
        surface.blit(label, (10, 10))
        # Draw Clock
        clock_text = datetime.datetime.now().strftime("%H:%M:%S")
        label = self.font2.render(clock_text, 1, (255,255, 255))
        surface.blit(label, (10, 50))
        # Draw temperature data
        temp1 = self.systemDriver.drivers[systemDriver.DRV_TEMPSENS1].temperature()
        hum1 = self.systemDriver.drivers[systemDriver.DRV_TEMPSENS1].humidity()
        temp_text = "{:.1f}".format(temp1) + texts.text_celsius1 + "{:.0f}".format(hum1) + "%"
        label = self.font3.render(temp_text, 1, (255,255, 255))
        surface.blit(label, (10, 130))
        # Draw touch screen text
        label = self.font0.render(texts.text_touchme, 1, (255,255, 255))
        surface.blit(label, (40, 210))
        # Draw kWh data
        cons1 = self.systemDriver.drivers[systemDriver.DRV_SAUNA].GetConsumption()
        money = self.systemDriver.drivers[systemDriver.DRV_SAUNA].GetMoney()
        temp_text = texts.text_lasttime + " {:.1f}".format(cons1) + texts.text_kwh1 + "{:.0f}".format(money) + texts.text_ft
        label = self.font0.render(temp_text, 1, (128,128, 128))
        surface.blit(label, (10, 180))
        #Check Door
        if self.doorDrv.GetState() != self.doorstate:
            self.doorstate = self.doorDrv.GetState()
            self.backlightTimer = 1600
            self.SetBacklight(1)

            if self.doorDrv.GetState():
                self.lightDrv.SwitchOn(1, 60)
            else:
                self.lightDrv.SwitchOn(0)
    
    def OnEvent(self, event):
        self.backlightTimer = 1600
        if event.type == pygame.MOUSEBUTTONUP:
            self.SetBacklight(1)
            return graphDriver.SC_DRIVER_MODESELECT
        else:
            return 0

    def SetBacklight(self, on):
        if platform.system() != 'Windows':
            if on:
                if self.currentBacklight != 1:
                    os.system("echo '1' > /sys/class/gpio/gpio252/value")
                    self.currentBacklight = 1
            else:
                if self.currentBacklight != 0:
                    os.system("echo '0' > /sys/class/gpio/gpio252/value")
                    self.currentBacklight = 0
            
