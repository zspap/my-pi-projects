# -*- coding: cp1250 -*-
import time
import pigpio
import hwDriverGeneralClass
import saunasimu
import texts

class tempSensorDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for temperature/humidity sensor"""
    def __init__(self, pi, gpio, gpioctrl):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.pi = pi;
        self.gpio = gpio
        self.gpioCtrl = gpioctrl
        self.bad_CS = 0 # checksum
        self.bad_TO = 0 # time-out
        self.rhum = -999
        self.rhumtemp = -999
        self.temp = -999
        self.temptemp = -999
        self.tov = None
        self.tick = 0
        self.bit = 40
        self.tickTimer = 0
        self.pi.set_mode(gpio, pigpio.INPUT)
        self.pi.set_pull_up_down(gpio, pigpio.PUD_OFF)
        self.cb = self.pi.callback(gpio, pigpio.EITHER_EDGE, self._cb)
        self.pi.set_mode(gpioctrl, pigpio.OUTPUT)
        self.pi.write(self.gpioCtrl, 1)
     
    def OnTick(self):
        """Called in every 0.5 seconds. Triggers a measurement cycle"""
        self.tickTimer = self.tickTimer + 1
        self.pi.write(self.gpioCtrl, 1)
        if self.tickTimer > 4:           # Can provide new and valid data in every 2 seconds only.
            #print ("Trigger:" + str(self.gpio))
            self.trigger()
            self.tickTimer = 0

        #self.temp = saunasimu.simuTemperature
        #self.rhum = saunasimu.simuHumidity 

        return 0

    def IsHWOK(self):
        return True
        if (self.bad_CS > 4) or (self.bad_TO > 4):
            self.rhum = -999
            self.temp = -999
            if (self.bad_TO > 4):
                self.ErrorMessage = texts.text_error_noanswer
            else:
                self.ErrorMessage = texts.text_error_wronganswer
            return False
        return True

    def _cb(self, gpio, level, tick):
        """
        Accumulate the 40 data bits.  Format into 5 bytes, humidity high,
        humidity low, temperature high, temperature low, checksum.
        """
        if level == 0:
            diff = pigpio.tickDiff(self.tick, tick)
            # edge length determines if bit is 1 or 0
            if diff >= 50:
                val = 1
            else:
                val = 0
            if self.bit >= 40: # message complete
                self.pi.set_watchdog(self.gpio, 0)
                self.bit = 40
                bad_TO = 0
                self.temp = self.temptemp
                self.rhum = self.rhumtemp
            elif self.bit >= 32: # in checksum byte
                self.CS  = (self.CS<<1)  + val
                if self.bit == 39:
                    # 40 bits received
                    self.pi.set_watchdog(self.gpio, 0)
                    total = self.hH + self.hL + self.tH + self.tL
                    if (total & 255) == self.CS: # is checksum ok
                        self.bad_CS = 0
                        self.rhumtemp = ((self.hH<<8) + self.hL) * 0.1
                        if self.tH & 128: # negative temperature
                            mult = -0.1
                            self.tH = self.tH & 127
                        else:
                            mult = 0.1
                        self.temptemp = ((self.tH<<8) + self.tL) * mult
                        self.tov = time.time()
                    else:
                        self.bad_CS += 1
                        #print(str(self.gpio) + "CRC Error.")
            elif self.bit >=24: # in temp low byte
                self.tL = (self.tL<<1) + val
            elif self.bit >=16: # in temp high byte
                self.tH = (self.tH<<1) + val
            elif self.bit >= 8: # in humidity low byte
                self.hL = (self.hL<<1) + val
            elif self.bit >= 0: # in humidity high byte
                self.hH = (self.hH<<1) + val
            else:               # header bits
                pass
    
            self.bit += 1
        elif level == 1:
            diff = pigpio.tickDiff(self.tick, tick)
            self.tick = tick
            if diff > 250000:
                self.bit = -2
                self.hH = 0
                self.hL = 0
                self.tH = 0
                self.tL = 0
                self.CS = 0
        else: # level == pigpio.TIMEOUT:
            # time out if less than 40 bits received
            if self.bit != 40:
                self.pi.set_watchdog(self.gpio, 0)
                self.bad_TO += 1
                self.pi.write(self.gpio, 1)
                self.pi.write(self.gpioCtrl, 0)                
                #print(str(self.gpio) + "Timeout.Bit:" + str(self.bit))

    def temperature(self):
        """Return current temperature."""
        return self.temp

    def humidity(self):
        """Return current relative humidity."""
        return self.rhum

    def staleness(self):
        """Return time since measurement made."""
        if self.tov is not None:
            return time.time() - self.tov
        else:
            return -999

    def bad_checksum(self):
        """Return count of messages received with bad checksums."""
        return self.bad_CS

    def timed_out(self):
        """Return count of messages which have timed out."""
        return self.bad_TO

    def trigger(self):
        """Trigger a new relative humidity and temperature reading."""
        #self.pi.write(self.gpio, 1)
        self.pi.set_mode(self.gpio, pigpio.OUTPUT)  # Start signal for min. 1ms
        self.pi.write(self.gpio, 0)
        time.sleep(0.001)
        self.pi.set_mode(self.gpio, pigpio.INPUT)   # Goes HIGH because of resistor.
        self.pi.set_watchdog(self.gpio, 50)

    def cancel(self):
        """Cancel the DHT22 sensor."""
        if self.cb != None:
            self.cb.cancel()

if __name__ == "__main__":

   pi = pigpio.pi()

   s = tempSensorDrv(pi, 28) # 28 - 29

   r = 0

   while True:

      r += 1

      s.trigger()

      time.sleep(0.2)

      print("{} {} {} {:3.2f} {} {}"
         .format(r, s.humidity(), s.temperature(), s.staleness(),
         s.bad_checksum(), s.timed_out()))

      time.sleep(1.8)

   s.cancel()

   pi.stop()
