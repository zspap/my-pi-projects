# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import texts
import logger


class faultScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 30)
        self.blinkCounter = 0
        self.iconFault = pygame.image.load('./artwork/iconFault.bmp')
        self.iconFault.set_colorkey((0,0,0))
        self.ErrorMessage = ''

    def InitScreen(self, errormsg):
        self.ErrorMessage = errormsg        
        logger.MainLogger.LogEvent("System Error: " + errormsg)
        
    def OnDraw(self, surface):
        # Draw text
        self.blinkCounter += 1
        if self.blinkCounter & 8:  
            surface.fill((255,0,0))

        label = self.font0.render(texts.text_system_error, 1, (255, 55, 55))
        surface.blit(label, (64, 4))
        label = self.font0.render(self.ErrorMessage, 1, (255, 255, 255))
        surface.blit(label, (32, 34))
        surface.blit(self.iconFault, (100, 80))
             
    def OnEvent(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            return graphDriver.SC_DRIVER_MAIN
        return 0

