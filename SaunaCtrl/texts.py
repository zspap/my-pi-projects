# -*- coding: cp1250 -*-

text_back = 'Vissza'
text_vent0 = 'Vent=0'
text_vent1 = 'Vent=1'
text_lmp0 = 'Lmp=0'
text_lmp1 = 'Lmp=1'
text_exit = 'Kil�p'
text_rgb0 = 'RGB=0' 
text_rgb1 = 'RGB=1' 
text_rgb2 = 'RGB=2' 
text_rplusplus = 'R++'
text_gplusplus = 'G++'
text_bplusplus = 'B++'
text_Inf0 = 'Inf=0'
text_Inf1 = 'Inf=1'
text_heating0 = 'F�t=0'
text_heating1 = 'F�t=1'
text_heating2 = 'F�t=2'
text_heating3 = 'F�t=3'
text_pump0 = 'Pum=0'
text_pump1 = 'Pum=1'
text_musicOFF = 'ZenKI'
text_musicON = 'ZenBE'
text_celsius1 = "�C  "
text_celsius2 = "�C"
text_kwh1 = "kWh = "
text_ft = "Ft"
text_temperature = "H�m�rs�klet:"
text_timetogo = "H�tralev� id�:"
text_cancel = 'M�gsem'
text_cleaning = 'Takar�t�s'
text_infra_button = 'Infra'
text_sanarium = 'Szan�rium [50�C]'
text_finnish1 = 'Finn szauna [70�C]'
text_finnish2 = 'Finn szauna [90�C]'
text_infra1 =   'Infra szauna [35�C]'
text_infra2 =   'Infra szauna [60�C]'
text_csilla =   'Csilla 100+ [100�C]'
text_diagnostics = 'Diagnosztika'
text_touchme = "(�rints meg az ind�t�shoz!)"
text_lasttime = "Legut�bb:"

text_diag_V =  "V:"
text_diag_L = " L:"
text_diag_F = " F:"
text_diag_P = " P:"
text_diag_H = " H:"
text_diag_I = " I:"

text_jan = 'Janu�r'
text_febr ='Febru�r'
text_mar = 'M�rcius'
text_apr = '�prilis'
text_may = 'M�jus'
text_jun = 'J�nius'
text_jul = 'J�lius'
text_aug = 'Augusztus'
text_sept ='Szeptember'
text_oct = 'Okt�ber'
text_nov = 'November'
text_dec = 'December'

text_please_close_door = "Csukd be az ajt�t!"

text_system_error = "Rendszerhiba!"

text_lamp1 = 'L�mpa'
text_color_therapy = 'F�nyter�pia'
text_color_automatic = 'Automata'

text_error_noanswer = "Szenzor nem v�laszol."
text_error_wronganswer = "Szenzor hib�s v�lasz."

text_setupstate = "Be�ll�t�s"
text_heateroff = "Kikapcsolt."
text_heatup = "Felf�t�s"
text_inframode = "Infra m�d"
text_dualmode = "Du�l m�d"
text_overheating = "T�lmeleged�s!"
text_finnishmode = "Finn m�d"
text_cleaningmode = "Takar�t�s m�d"
text_diagmode = "Diagnosztika"
text_errormode = "Probl�ma"
text_sensorfauilure = "Szenzorhiba!"
text_offmode = "Kikapcsolt."
text_pumperror = "Pumparendszer hiba."
text_infraerror = "Infrasug�rz� hiba."
text_nondefined_error = "Nem defini�lt hiba."
text_heater_error = "F�t�rendszer hiba."

text_minutes = 'p'

path_Kitaro = './Music/Kitaro/'

