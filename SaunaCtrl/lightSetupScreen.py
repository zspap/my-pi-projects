# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import pygbutton
import texts


color_array1 = [7, 6, 4, 5, 1, 3, 2, 7]
color_array2 = [0, 7, 6, 4, 5, 1, 3, 2]

class lightSetupScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver

        self.autoCloseCounter = 1600
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 30)
        self.font1 = pygame.font.Font(None, 80)

        self.buttonLamp = pygbutton.PygButton((0, 0, 100, 60), texts.text_lamp1, fgcolor=(255,255,255), bgcolor = (0,0,32))
        self.buttonOk =  pygbutton.PygButton((220, 0, 100, 100), normal='./artwork/iconButtonOK.bmp', down='./artwork/iconButtonOKAct.bmp', highlight='./artwork/iconButtonOK.bmp', keycolor = (0,0,0))
        self.buttonRGB = pygbutton.PygButton((110, 0, 100, 60), texts.text_color_therapy, fgcolor=(255,255,255), bgcolor = (0,0,32))
        self.buttonAutoManual =  pygbutton.PygButton((220, 110, 100, 60), texts.text_color_automatic, fgcolor=(255,255,255), bgcolor = (0,0,32))
        self.buttonR =  pygbutton.PygButton((130, 120, 60, 30), '', bgcolor = (255,0,0))
        self.buttonG =  pygbutton.PygButton((130, 150, 60, 30), '', bgcolor = (0,255,0))
        self.buttonB =  pygbutton.PygButton((130, 180, 60, 30), '', bgcolor = (0,0,255))
        self.buttonW =  pygbutton.PygButton((130, 210, 60, 30), '', bgcolor = (255,255,255))
        self.buttonPlus =  pygbutton.PygButton((70, 80, 60, 30), '+', fgcolor = (255,255,25), bgcolor = (0,0,0))
        self.buttonMinus =  pygbutton.PygButton((130, 80, 60, 30), '-', fgcolor = (255,255,25), bgcolor = (0,0,0))
        self.background = pygame.image.load('./artwork/backgroundLight.bmp')
        self.background.set_alpha(128)

        self.palette = pygame.Surface((128,128))
        for y in range(4, 63, 1):
            for x in range(0, 63):
                corr = x & 7
                r = (color_array1[x >> 3]      & 1) * corr + ((color_array2[x >> 3]      & 1) * (7-corr))
                g = ((color_array1[x >> 3]>>1) & 1) * corr + (((color_array2[x >> 3]>>1) & 1) * (7-corr))
                b = ((color_array1[x >> 3]>>2) & 1) * corr + (((color_array2[x >> 3]>>2) & 1) * (7-corr))
                
                color = (r * y / 2, g * y / 2, b * y / 2)

                self.palette.set_at((x * 2    , y * 2), color)                                 
                self.palette.set_at((x * 2 + 1, y * 2), color)                                 
                self.palette.set_at((x * 2    , y *2 + 1), color)                                 
                self.palette.set_at((x * 2 + 1, y *2 + 1), color)                                 
            
    def InitScreen(self, rgbmode, r, g, b, lamp):
        self.rgbMode = rgbmode
        self.R = r
        self.G = g
        self.B = b
        self.lampMode = lamp

        if not self.lampMode:
            self.buttonLamp.bgcolor = (0, 0, 32)
        else:
            self.buttonLamp.bgcolor = (0, 255, 0)

        if not self.rgbMode:
            self.buttonRGB.bgcolor = (0, 0, 32)
        else:
            self.buttonRGB.bgcolor = (0, 255, 0)

        if self.rgbMode == 2:
            self.buttonAutoManual.bgcolor = (0, 0, 32)
        else:
            self.buttonAutoManual.bgcolor = (0, 255, 0)

    def OnDraw(self, surface):
        surface.blit(self.background, (0, 0))  
        if self.rgbMode:
            # Draw Color Sample
            if self.rgbMode == 2:
                pygame.draw.rect(surface, (self.R, self.G, self.B), (220, 180, 100, 60), 0)
                # Draw Color Select
                surface.blit(self.palette, (0,120))
                self.buttonR.draw(surface)
                self.buttonG.draw(surface)
                self.buttonB.draw(surface)
                self.buttonW.draw(surface)
                self.buttonPlus.draw(surface)
                self.buttonMinus.draw(surface)
            self.buttonAutoManual.draw(surface)
        # Draw Buttons
        self.buttonLamp.draw(surface)
        self.buttonOk.draw(surface)
        self.buttonRGB.draw(surface)
       
        
        self.autoCloseCounter -= 1
        if self.autoCloseCounter < 0:
             pygame.event.post(pygame.event.Event(USEREVENT, {'x': 1, 'y':1}))
        
    def OnEvent(self, event):
        if event.type == USEREVENT:
            self.autoCloseCounter = 1600
            return graphDriver.SC_DRIVER_MAIN
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.autoCloseCounter = 1600
            relx, rely = pygame.mouse.get_pos()
            if (relx < 128) and (rely > 112):
                color = self.palette.get_at((relx, rely - 112))
                (self.R, self.G, self.B) = (color.r, color.g, color.b)
                self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
                
        if 'click' in self.buttonOk.handleEvent(event):
            self.autoCloseCounter = 1600
            # Save values
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(self.lampMode)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
            
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonLamp.handleEvent(event):
            self.autoCloseCounter = 1600
            if self.lampMode:
                self.lampMode = 0
                self.buttonLamp.bgcolor = (0, 0, 32)
            else:
                self.lampMode = 1
                self.buttonLamp.bgcolor = (0, 255, 0)
            # Save values
            self.systemDriver.drivers[systemDriver.DRV_LAMP].SwitchOn(self.lampMode)
                
        if 'click' in self.buttonRGB.handleEvent(event):
            self.autoCloseCounter = 1600
            if self.rgbMode:
                self.rgbMode = 0
                self.buttonRGB.bgcolor = (0, 0, 32)
            else:
                self.rgbMode = 1
                self.buttonRGB.bgcolor = (0, 255, 0)
            # Save values
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
            
        if 'click' in self.buttonAutoManual.handleEvent(event):
            self.autoCloseCounter = 1600
            if self.rgbMode == 1:
                self.rgbMode = 2
                self.buttonAutoManual.bgcolor = (0, 0, 32)
            else:
                self.rgbMode = 1
                self.buttonAutoManual.bgcolor = (0, 255, 0)
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
                
        if 'click' in self.buttonR.handleEvent(event):
            self.autoCloseCounter = 1600
            self.R = 255
            self.G = 0
            self.B = 0
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
        if 'click' in self.buttonG.handleEvent(event):
            self.autoCloseCounter = 1600
            self.R = 0
            self.G = 255
            self.B = 0
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
        if 'click' in self.buttonB.handleEvent(event):
            self.autoCloseCounter = 1600
            self.R = 0
            self.G = 0
            self.B = 255
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
        if 'click' in self.buttonW.handleEvent(event):
            self.autoCloseCounter = 1600
            self.R = 255
            self.G = 255
            self.B = 255
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
        if 'click' in self.buttonPlus.handleEvent(event):
            self.autoCloseCounter = 1600
            self.R *= 1.25
            self.G *= 1.25
            self.B *= 1.25
            if self.R > 255:
                self.R = 255
            if self.G > 255:
                self.G = 255
            if self.B > 255:
                self.B = 255
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
        if 'click' in self.buttonMinus.handleEvent(event):
            self.autoCloseCounter = 1600
            self.R /= 1.25
            self.G /= 1.25
            self.B /= 1.25
            self.systemDriver.drivers[systemDriver.DRV_RGB].SwitchOn(self.rgbMode, self.R, self.G, self.B)
            
        return 0

   


