import time
import pigpio
import hwDriverGeneralClass
import texts

class pumpDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for automatic drain pump control"""
    def __init__(self, pi, gpioOut, gpioIn):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.minRunTime = 0
        self.maxRunTime = 0
        self.pi = pi
        self.gpioO = gpioOut
        self.gpioI = gpioIn
        self.pi.set_mode(self.gpioO, pigpio.OUTPUT)
        self.pi.set_pull_up_down(self.gpioO, pigpio.PUD_OFF)
        self.pi.write(self.gpioO, 0)        
        self.pi.set_mode(self.gpioI, pigpio.INPUT)
        self.pi.set_pull_up_down(self.gpioI, pigpio.PUD_DOWN)
        self.currentState = 0   #  -1: Forced Off ,   0: AutoOFF,   1: AutoRunning  2: Forced ON,    3: Error

    def OnTick(self):
        if self.currentState == 0: # Currently switched off
            # Check input line
            if self.pi.read(self.gpioI) == 0:
                # Anti-prelling
                time.sleep(0.100)
                if self.pi.read(self.gpioI) == 0:
                    # Water level is high. Switch ON pump.
                    self.minRunTime = 0
                    self.maxRunTime = 0
                    self.currentState = 1
                    self.pi.write(self.gpioO, 1)
                    return 1
                else:
                    print("Input hazard")
            return 0
        elif self.currentState == 1 :
            if self.pi.read(self.gpioI) == 0:
                # Water level is high, but pump is running.
                self.maxRunTime = self.maxRunTime + 1
                if self.minRunTime > 2400: # 20 minutes
                    self.currentState = 3
                    self.pi.write(self.gpioO, 0)
                    self.ErrorMessage = texts.text_pumperror
                    return 0
                return 1
            else:
                # Water level is low, pump is running.
                self.minRunTime = self.minRunTime + 1
                if self.minRunTime > 40: # 20 seconds
                    self.currentState = 0
                    self.pi.write(self.gpioO, 0)
                    return 0
                return 1
        elif  self.currentState ==2 :
            # Forced ON
            self.pi.write(self.gpioO, 1)
            return 1
        elif  self.currentState ==-1 :
            # Forced OFF
            self.pi.write(self.gpioO, 0)
            return 0
        else:   # Error
            self.pi.write(self.gpioO, 0)
        return 0

    def IsHWOK(self):
        if self.currentState == 3:
            return False
        return True

    def SwitchOn(self, on):
        if on == 1:  # Automata ON
            self.minRunTime = 0
            self.maxRunTime = 0
            self.currentState = 1
            self.pi.write(self.gpioO, 1)
        elif on == 2: # Forced ON
            self.minRunTime = 0
            self.maxRunTime = 0
            self.currentState = 2
            self.pi.write(self.gpioO, 1)
        elif on == -1: #Forced OFF
            self.minRunTime = 0
            self.maxRunTime = 0
            self.currentState = -1
            self.pi.write(self.gpioO, 0)
        else: # Automata OFF
            self.currentState = 0
            self.pi.write(self.gpioO, 0)

    def GetWaterLevel(self):
        return self.pi.read(self.gpioI) == 0
    
    def GetState(self):
        return self.currentState

