# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import texts



class closeDoorScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        self.doorDrv = sysDriver.drivers[systemDriver.DRV_DOOR]
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 30)
        self.blinkCounter = 0
        self.iconDoorBig = pygame.image.load('./artwork/iconDoorBig.bmp')
        self.iconDoorBig.set_colorkey((0,0,0))
        
    def OnDraw(self, surface):
        # Draw text
        self.blinkCounter += 1
        if self.blinkCounter & 8:  
            surface.fill((255,0,0))

        label = self.font0.render(texts.text_please_close_door, 1, (255,255, 255))
        surface.blit(label, (64, 4))
        surface.blit(self.iconDoorBig, (100, 40))

        if not self.doorDrv.GetState():
             pygame.event.post(pygame.event.Event(pygame.MOUSEBUTTONUP, {'x': 1, 'y':1}))
             
    def OnEvent(self, event):
        if event.type == pygame.MOUSEBUTTONUP:
            return graphDriver.SC_DRIVER_MAIN
        if not self.doorDrv.GetState():
            return graphDriver.SC_DRIVER_MAIN
        else:
            return 0
