# -*- coding: cp1250 -*-

import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *
import screenGeneralClass
import systemDriver
import graphDriver
import datetime
import pygbutton
import texts
import logger

SET_PARAM_MODE_TEMP = 0
SET_PARAM_MODE_VOL = 1
SET_PARAM_MODE_TIME = 2


class setValueScreen(screenGeneralClass.screenGeneralClass):
    def __init__(self, scDriver, sysDriver):
        screenGeneralClass.screenGeneralClass.__init__(self, sysDriver)
        self.screenDriver = scDriver
        self.saunaDrv = self.systemDriver.drivers[systemDriver.DRV_SAUNA]

        self.autoCloseCounter = 1600
        
        pygame.font.init()
        self.font0 = pygame.font.SysFont(None, 30)
        self.font1 = pygame.font.Font(None, 80)

        self.OperationMode = 0
        self.TextToWrite = ''
        self.MinValue = 0
        self.MaxValue = 100
        self.CurrentValue = 0
        self.Step = 1
        self.Extra = None

        self.buttonMinus = pygbutton.PygButton((0, 155, 100, 60), normal='./artwork/iconButtonMinus.bmp', down='./artwork/iconButtonMinusAct.bmp', highlight='./artwork/iconButtonMinus.bmp', keycolor = (0,0,0))
        self.buttonPlus = pygbutton.PygButton((220, 155, 100, 60), normal='./artwork/iconButtonPlus.bmp', down='./artwork/iconButtonPlusAct.bmp', highlight='./artwork/iconButtonPlus.bmp', keycolor = (0,0,0))
        self.buttonOk =  pygbutton.PygButton((110, 140, 100, 100), normal='./artwork/iconButtonOK.bmp', down='./artwork/iconButtonOKAct.bmp', highlight='./artwork/iconButtonOK.bmp', keycolor = (0,0,0))
        self.buttonExtra =  pygbutton.PygButton((220, 0, 100, 50), texts.text_infra_button, font = pygame.font.Font('freesansbold.ttf', 40), fgcolor= (0,0,0))

        self.iconHeat = pygame.image.load('./artwork/iconThermo.bmp')
        self.iconHeat.set_colorkey((0,0,0))
        self.iconVolume = pygame.image.load('./artwork/iconVolume.bmp')
        self.iconVolume.set_colorkey((0,0,0))
        self.iconTime = pygame.image.load('./artwork/iconTime.bmp')
        self.iconTime.set_colorkey((0,0,0))

        self.backgroundFire = pygame.image.load('./artwork/backgroundFire.bmp')
        self.backgroundFire.set_alpha(128)
        self.backgroundTime = pygame.image.load('./artwork/backgroundTime.bmp')
        self.backgroundTime.set_alpha(128)

    def InitScreen(self,text, mode, minval, maxval, curval, step, extra = None):
        self.OperationMode = mode;
        self.TextToWrite = text
        self.MinValue = minval
        self.MaxValue = maxval
        self.CurrentValue = curval
        self.Step = step
        self.Extra = extra
        
    def OnDraw(self, surface):
        if self.OperationMode == SET_PARAM_MODE_TEMP:
            surface.blit(self.backgroundFire, (0, 0))
        else:
            surface.blit(self.backgroundTime, (0, 0))
        # Draw Text
        label = self.font0.render(self.TextToWrite, 1, (255,255,255))
        surface.blit(label, (2, 2))
        # Draw Buttons
        self.buttonPlus.draw(surface)
        self.buttonMinus.draw(surface)
        self.buttonOk.draw(surface)
        # Draw Value/Visualisation
        if self.OperationMode == SET_PARAM_MODE_TEMP:
            surface.blit(self.iconHeat, (0, 40))
            temp_text = "{:.1f}".format(self.CurrentValue) + texts.text_celsius2
            label = self.font1.render(temp_text, 1, (self.CurrentValue * 2, 64, 255 - self.CurrentValue * 2))
            surface.blit(label, (100, 60))
            if self.Extra == saunaControl.MODE_INFRA:
               self.buttonExtra.bgcolor = (0,255,0)
            else:
               self.buttonExtra.bgcolor = (128,0,0)
            self.buttonExtra.draw(surface)
        elif self.OperationMode == SET_PARAM_MODE_VOL:
            surface.blit(self.iconValume, (0, 40))
            temp_text = str(self.CurrentValue) + "%"
            label = self.font1.render(temp_text, 1, (255,255,255))
            surface.blit(label, (100, 60))           
        else:
            surface.blit(self.iconTime, (0, 40))
            a = datetime.timedelta(seconds = self.CurrentValue)
            label = self.font1.render(str(a), 1, (255,255, 255))
            surface.blit(label, (100, 60))         

        self.autoCloseCounter -= 1
        if self.autoCloseCounter < 0:
             pygame.event.post(pygame.event.Event(USEREVENT, {'x': 1, 'y':1}))
        
    def OnEvent(self, event):
        self.autoCloseCounter = 1600
        if event.type == USEREVENT:
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonOk.handleEvent(event):
            if self.OperationMode == SET_PARAM_MODE_TEMP:
                self.saunaDrv.SetTemperature(self.CurrentValue) 
                logger.MainLogger.LogEvent("Set Temperature to: " + str(self.CurrentValue))
            elif self.OperationMode == SET_PARAM_MODE_VOL:
                self.systemDriver.drivers[systemDriver.DRV_SOUND].SetVolume(self.CurrentValue)
            else:
                 self.saunaDrv.SetTimeToGo(self.CurrentValue)
                 logger.MainLogger.LogEvent("Set Time to Go to: " + str(self.CurrentValue))
            if self.Extra != None:
                self.saunaDrv.SetSaunaMode(self.Extra)
            return graphDriver.SC_DRIVER_MAIN
        if 'click' in self.buttonPlus.handleEvent(event):
            self.autoCloseCounter = 1600
            self.CurrentValue += self.Step
            if self.CurrentValue > self.MaxValue:
               self.CurrentValue = self.MaxValue
        if 'click' in self.buttonMinus.handleEvent(event):
            self.autoCloseCounter = 1600
            self.CurrentValue -= self.Step
            if self.CurrentValue < self.MinValue:
               self.CurrentValue = self.MinValue
        if 'click' in self.buttonExtra.handleEvent(event):
            self.autoCloseCounter = 1600
            if self.Extra == saunaControl.MODE_INFRA:
                self.Extra = saunaControl.MODE_HEAT
            else:
                self.Extra = saunaControl.MODE_INFRA
               
        return 0

   
