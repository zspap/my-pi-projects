# -*- coding: cp1250 -*-
import time
import pigpio
import hwDriverGeneralClass
import texts

class heaterDrv(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for main heater module"""
    def __init__(self, pi, gpio1, gpio2):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        self.safetyTimer = 0
        self.pi = pi
        self.gpio1 = gpio1
        self.gpio2 = gpio2
        self.pi.set_mode(gpio1, pigpio.OUTPUT)
        self.pi.set_mode(gpio2, pigpio.OUTPUT)
        self.currentState = 0    # 0: OFF.  1: LOW   2: HIGH,   3: FULL Power

        self.OperationTime = 0

    def OnTick(self):
        self.safetyTimer = self.safetyTimer + 1
        if not self.IsHWOK():         # Force off heater.
            self.pi.write(self.gpio, 0)
            self.pi.write(self.gpio, 0)
        else:
            if self.currentState == 3:
                self.OperationTime = self.OperationTime + 3;
            elif self.currentState == 2:
                self.OperationTime = self.OperationTime + 2;
            elif self.currentState == 1:
                self.OperationTime = self.OperationTime + 1;
            else:
                pass

        return self.currentState
    
    def IsHWOK(self):
        if (self.currentState > 0) and (self.safetyTimer > 7200):
            self.ErrorMessage = texts.text_heater_error
            return False
        return True

    def SwitchOn(self, on):
        if self.currentState != on:
           self.safetyTimer = 0
        self.currentState = on
        if on == 3:
            self.pi.write(self.gpio1, 1)
            self.pi.write(self.gpio2, 1)
        elif on == 2:
            self.pi.write(self.gpio1, 0)
            self.pi.write(self.gpio2, 1)
        elif on == 1:
            self.pi.write(self.gpio1, 1)
            self.pi.write(self.gpio2, 0)
        else:
            self.pi.write(self.gpio1, 0)
            self.pi.write(self.gpio2, 0)

    def GetState(self):
        return self.currentState

    def GetSetOperationTime(self, clear):
        if clear:
            self.OperationTime = 0    
        return self.OperationTime
