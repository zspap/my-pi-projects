# -*- coding: cp1250 -*-
import pygame, pygbutton, sys
from pygame.locals import *
from systemDriver import *


class screenGeneralClass():
    """General -abstract- class for display screens"""
    def __init__(self, sysDrv):
        self.systemDriver = sysDrv

    def OnDraw(self, surface):
        pass

    def OnEvent(self, event):
        pass
    
