import time
import datetime
import hwDriverGeneralClass

MainLogger = None

class loggerDriver(hwDriverGeneralClass.hwDriverGeneralClass):
    """Hardware driver for sound subsystem"""
    def __init__(self):
        hwDriverGeneralClass.hwDriverGeneralClass.__init__(self)
        global MainLogger
        self.logFile = open ("mainlog.txt", "a", 1)
        self.LogEvent("Start logging.")   
        MainLogger = self        
                   
    def CloseLog(self):
        self.LogEvent("Close Log.")
        self.logFile.close()
        
    def OnTick(self):
        return 0
        
    def IsHWOK(self):
        return True
        
    def printDatetime(self):   
        clock_text = datetime.datetime.now().strftime("%Y.") + datetime.datetime.now().strftime("%m") + datetime.datetime.now().strftime(".%d ")
        clock_text += datetime.datetime.now().strftime("%H:%M:%S")
        self.logFile.write(clock_text + " - ")
    
    def LogEvent(self, usertext):
        self.printDatetime()
        self.logFile.write(usertext + '\n')
        self.logFile.flush()
      
      
